import numpy as np
import matplotlib.pyplot as plt
import Legendre as lg
import math
import GolubFischer

#============================ Analytical test function
def F0(mu):
    a = 20
    b = 0.25*math.pi
    # a = 4
    # b = 0.0 * math.pi

    xmin = b - math.pi/2/a
    xmax = b + math.pi/2/a

    x = math.acos(mu)

    if ((x < xmin) or (x > xmax)):
        return 0
    else:
        return 0.4*math.cos(a*(x-b))

def F1(mu):
    return math.exp(1*(mu+1)/2)



#============================ Function product with Legendre
def FPl(F,x,l):
    return F(x)*lg.Legendre(l,x)

#============================ Simple Riemann integration
#
# F = Function of x to integrate
# a = xmin
# b = xmax
# N = Number of points (default=1000)
# l = Legendre moment to pass (default=-1 not passed)
#
def RiemannIntegration(F,a,b,N=1000,l=-1):
    dx = (b-a)/N

    value = 0.0
    for i in range(0,N):
        x = a + 0.5*dx + i*dx    #midpoint rule
        if l<0:
            value = value + F(x)*dx
        else:
            value = value + FPl(F,x,l) * dx

    return value

#======================= Legendre expansion of function
def LegendreExp(F,L,N_p):
    fl = np.zeros(L + 1)
    for l in range(0, L + 1):
        fl[l] = RiemannIntegration(F, -1, 1, 1000, l)

    #print(fl)

    x2 = np.linspace(-1, 1, N_p)
    y2 = np.zeros(N_p)
    for i in range(0, N_p):
        for l in range(0, L + 1):
            y2[i] = y2[i] + (2 * l + 1) * (1.0/2.0)\
                    * fl[l] * lg.Legendre(l, x2[i])

    return x2,y2,fl

#======================= Function evaluation given moments
def Fdstar(Md,x):
    Nstar = np.size(Md)
    value = 0.0
    for ell in range(0,Nstar):
        value+=(2*ell+1)*0.5*Md[ell]*lg.Legendre(ell,x)

    return value

plt.figure(1)
#======================= Analytical function
N_p=2000
F = F0
x1 = np.linspace(-1,1,N_p)
y1 = np.zeros(N_p)


for i in range(0,N_p):
    y1[i] = F(x1[i])

plt.plot(x1,y1,label='Analytical function')

#======================= Compute Legendre expansion
L=3
x2,y2,fl = LegendreExp(F,L,N_p)

plt.plot(x2,y2,label='L='+str(L)+' Legendre expansion')


#======================= Find orthogonal polys
Mell = fl

N = np.size(Mell)-1
print("Number of moments supplied: %d, (2*n-1)=%d" %(N+1,N))
n = int((N+1)/2)
print("Therefore n=%d" %n)
print("Mell[0 to %d]:" %N)
print(Mell)
#
# a = np.zeros(2*n)
# b = np.zeros(2*n)
# c = np.zeros(2*n)
#
# for j in range(0,2*n):
#     a[j] = 0.0
#     b[j] = j/(2*j+1)
#     c[j] = (j+1)/(2*j+1)
#
# print("a,b,c [0 to %d]:" %N)
# print(a)
# print(b)
# print(c)
#
# alpha,beta = Gautschi.MCA(Mell,a,b,c)
# print("alpha[0 to %d]:" %n)
# print(alpha)
# print("beta[0 to %d]:" %n)
# print(beta)
#
# #======================= Find roots
# xn,wn = Gautschi.RootsOrtho(n,alpha,beta)

xn,wn = GolubFischer.GetDiscreteScatAngles(Mell)

yn=np.zeros(np.size(xn))
print("xn[0 to %d]:" %(n-1))
print(xn)
print("wn[0 to %d]:" %(n-1))
print(wn)


#======================= Test moments
Md = np.zeros(2*n)
for k in range(0,2*n):
    for i in range(0,n):
        Md[k]+=wn[i]*lg.Legendre(k,xn[i])

print("Melld Reconstructed [0 to %d]:" %N)
print(Md)

#======================= Test distribution
x3 = np.zeros(n)
y3 = np.zeros(n)
for i in range(0,n):
    x3[i] = xn[i]
    y3[i] = Fdstar(Md,x3[i])

plt.scatter(x3,y3,marker='D',color='black',
            label='Gauss-quadrature, moments reconstructed')


plt.xlabel('Scattering cosine $\mu$')
plt.ylabel('Probability')
plt.xlim(-1,1)
plt.ylim(np.min(y2),np.max(y1)*1.2)
plt.legend()

plt.savefig("GaussquadTest.png")
plt.show()


print("End of program")

