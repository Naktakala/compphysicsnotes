import numpy as np
import matplotlib.pyplot as plt
import Legendre as lg
import math

#============================ Analytical test function
def F0(mu):
    a = 20
    b = 0.25*math.pi

    xmin = b - math.pi/2/a
    xmax = b + math.pi/2/a

    x = math.acos(mu)

    if ((x < xmin) or (x > xmax)):
        return 0
    else:
        return 0.4*math.cos(a*(x-b))

#============================ Function product with Legendre
def FPl(F,x,l):
    return F(x)*lg.Legendre(l,x)

#============================ Simple Riemann integration
#
# F = Function of x to integrate
# a = xmin
# b = xmax
# N = Number of points (default=1000)
# l = Legendre moment to pass (default=-1 not passed)
#
def RiemannIntegration(F,a,b,N=1000,l=-1):
    dx = (b-a)/N

    value = 0.0
    for i in range(0,N):
        x = a + 0.5*dx + i*dx    #midpoint rule
        if l<0:
            value = value + F(x)*dx
        else:
            value = value + FPl(F,x,l) * dx

    return value

#======================= Legendre expansion of function
def LegendreExp(F,L,N_p):
    fl = np.zeros(L + 1)
    for l in range(0, L + 1):
        fl[l] = RiemannIntegration(F0, -1, 1, 1000, l)

    x2 = np.linspace(-1, 1, N_p)
    y2 = np.zeros(N_p)
    for i in range(0, N_p):
        for l in range(0, L + 1):
            y2[i] = y2[i] + (2 * l + 1) * 0.5 * fl[l] * lg.Legendre(l, x2[i])

    return x2,y2

def ComputeCDF(x,y):
    N_p = np.size(x)

    sump = 0.0
    for i in range(0,N_p):
        sump = sump + y[i]

    for i in range(0,N_p):
        y[i] = y[i]/sump

    y_out = np.zeros(N_p)
    for i in range(0,N_p):
        for j in range(0,i+1):
            y_out[i] = y_out[i]+y[j]



    return x,y_out


plt.figure(1)
#======================= Analytical function
N_p=200
x1 = np.linspace(-1,1,N_p)
y1 = np.zeros(N_p)


for i in range(0,N_p):
    y1[i] = F0(x1[i])

plt.plot(x1,y1,label='Analytical function')

#======================= Legendre expansion
L = 8
x2,y2 = LegendreExp(F0,L,N_p)
plt.plot(x2,y2,label='L='+str(L))
L = 16
x2,y2 = LegendreExp(F0,L,N_p)
plt.plot(x2,y2,label='L='+str(L))
L = 32
x2,y2 = LegendreExp(F0,L,N_p)
plt.plot(x2,y2,label='L='+str(L))

plt.xlabel('Scattering cosine $\mu$')
plt.ylabel('Function value')
plt.legend()
plt.savefig("LegendreExpansion.png")

#======================= CDFs
plt.figure(2)
x1,y1 = ComputeCDF(x1,y1)
plt.plot(x1,y1,label='Analytical function')
L = 8
x2,y2 = ComputeCDF(x2,y2)
plt.plot(x2,y2,label='L='+str(L))

plt.xlabel('Scattering cosine $\mu$')
plt.ylabel('Cumulative Probability Density')
plt.legend()
plt.savefig("CDF.png")

plt.show()


print("Hello world")

