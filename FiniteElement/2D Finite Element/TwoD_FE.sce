clear 
clc

xn=[1; 5; 3]
yn=[0;-2; 2]

va = [xn(2)-xn(1) yn(2)-yn(1) 0]';
vb = [xn(3)-xn(1) yn(3)-yn(1) 0]';

xi = va/norm(va);
nu = cross([0 0 1],xi')'

T1 = [xi'; nu'; [0 0 1]]

var = T1*va;
vbr = T1*vb;

slope = vbr(1)/vbr(2);
scale = 1/vbr(2)

T2 = [1/norm(va) -slope/norm(va) 0;
      0 scale 0;
      0 0 1]

var = T2*var;
vbr = T2*vbr;

N=200
dx=max(xn)-min(xn);
dy=max(yn)-min(yn);
x=linspace(min(xn),max(xn),N)
y=linspace(min(yn),max(yn),N)

xy=zeros(N*N,3)
xyt=zeros(N*N,3)

v0 = [min(xn) min(yn) 0]'

pure_integral=0.0;
natu_integral=0.0;

k=0;
for i=1:N
    for j=1:N
        k=k+1
        v = [x(i)-xn(1) y(j)-yn(1) 0]';
        vt=T1*v;
        vt=T2*vt;
        
        xin = (vt(1)<=1) & (vt(1)>=0);
        yin = (vt(2)<=1) & (vt(2)>=0);
        ins = (vt(2)<=(1-vt(1)));
        
        if (xin & yin & ins) then
            xy(k,1) = v(1)+xn(1);
            xy(k,2) = v(2)+yn(1);
            
            pure_integral = pure_integral+dx*dy;
            natu_integral = natu_integral+(xn(2)-xn(1))*( yn(3)-yn(1) )*(1/N/N);
            
            xyt(k,1) = vt(1);
            xyt(k,2) = vt(2);
        end
        
    end
end
//natu_integral=(xn(2)-xn(1))*( yn(3)-yn(1) );

disp(pure_integral)
disp(natu_integral)


//#########################################################
scf(0)
clf(0)

scatter(xy(:,1),xy(:,2),4)
scatter(xyt(:,1),xyt(:,2),4)
//xarrows([x(1) x(2)],[y(1) y(2)],2)
//xarrows([x(1) x(3)],[y(1) y(3)],2)


xarrows([0 1],[0 0],2,5)
xarrows([0 0],[0 1],2,2)

xarrows([xn(1) xn(2)],[yn(1) yn(2)],0)
xarrows([xn(2) xn(3)],[yn(2) yn(3)],0)
xarrows([xn(3) xn(1)],[yn(3) yn(1)],0)




a=gca()
a.data_bounds=[-1,-3; 6,3]


//scf(1)
//clf(1)
//
//scatter3(nodes(:,1),nodes(:,2),values')
//plot3d([nodes(:,1); nodes(1,1)],[nodes(:,2); nodes(1,2)],[values';values(1)])
//plot3d([nodes(:,1); nodes(1,1)],[nodes(:,2); nodes(1,2)],[0.1;0.1;0.1;0.1])
//
//a=gca()
//a.data_bounds=[0,0,0; 6,6,10]
//
//scf(2)
//clf(2)
//
//plot3d(x,y,z)
//a=gca()
//a.data_bounds=[0,0,0; 6,6,10]
