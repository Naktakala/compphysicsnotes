clear
clc

//Define triangle
xn=[1; 5; 3]
yn=[0;-2; 2]

//Calculate side vectors
i=1; x1=[xn(i) yn(i) 1]';
i=2; x2=[xn(i) yn(i) 1]';
i=3; x3=[xn(i) yn(i) 1]';

//Calculate transform matrix
x12 = x2-x1;
x13 = x3-x1;

vx = x12/norm(x12);
vy = cross([0 0 1]',vx);

Trot = [vx';vy';[0 0 1]];

x1t = Trot*(x1-x1);
x2t = Trot*(x2-x1);
x3t = Trot*(x3-x1);

x1t(3)=1;
x2t(3)=1;
x3t(3)=1;

x12t=x2t-x1t
x13t=x3t-x1t

T=[x12t(:) x13t(:) [0;0;1]]

//Coefficients
A=[x1t';x2t';x3t'];
b1=[1 0 0]';
b2=[0 1 0]';
b3=[0 0 1]';
c1=linsolve(A,-b1);
c2=linsolve(A,-b2);
c3=linsolve(A,-b3);


//Calculate points for plotting
N=200;
xi=linspace(0,1,N);
nu=linspace(0,1,N);
dnatural = [1/N 1/N 0]';
dx = x12t(1)*dnatural(1);
dy = x13t(2)*dnatural(2);




xi_nu = zeros(N*N,2);
xy    = zeros(N*N,2);
z1    = zeros(N*N,1);
z2    = zeros(N*N,1);
z3    = zeros(N*N,1);
disp(size(z1))

int_z1 = 0.0;
k=0;
for i=1:N
    for j=1:N
        k=k+1;
        if (nu(j)<=(1-xi(i))) then
            xi_nu(k,:)=[xi(i) nu(j)]
            v = [xi(i) nu(j) 0]';
            vt = T*v+x1t;
            xy(k,:)=vt(1:2)';
            z1(k)=c1(1)*vt(1)+c1(2)*vt(2)+c1(3);
            z2(k)=c2(1)*vt(1)+c2(2)*vt(2)+c2(3);
            z3(k)=c3(1)*vt(1)+c3(2)*vt(2)+c3(3);
            
            int_z1 = int_z1+dx*dy*z1(k);
        end
    end
end

printf("Natrual Integral = %f\n",int_z1)

//Numerical integration of function part 1
num_intgl = 0.0;
xr=linspace(0,x3t(1),N)
yr=linspace(0,x3t(2),N)
dx=x3t(1)/N;
dy=x3t(2)/N;
xyr = zeros(N*N,2);
zr  = zeros(N*N,1);
k=0;
for i=1:N
    for j=1:N
        k=k+1;
        if (yr(j)  <=  (x3t(2)*xr(i)/x3t(1)      )   ) then
            xyr(k,:)=[xr(i) yr(j)]
            zr(k)=c1(1)*xr(i)+c1(2)*yr(j)+c1(3);
        end
        num_intgl=num_intgl+dx*dy*zr(k);
    end
end

//Numerical integration of function part 2
xr2=linspace(x3t(1),x2t(1),N)
yr2=linspace(0,x3t(2),N)
dx=(x2t(1)-x3t(1))/N;
dy=x3t(2)/N;
xyr2 = zeros(N*N,2);
zr2  = zeros(N*N,1);
m=(x2t(2) -x3t(2))/( x2t(1) - x3t(1)    )
c=x2t(2) - m*x2t(1);
k=0;
for i=1:N
    for j=1:N
        k=k+1;
        if (yr2(j)  <=  ( m*xr2(i)+c     )   ) then
            xyr2(k,:)=[xr2(i) yr2(j)]
            zr2(k)=c1(1)*xr2(i)+c1(2)*yr2(j)+c1(3);
        end
        num_intgl=num_intgl+dx*dy*zr2(k);
    end
end

printf("Numerical Integral = %f\n",num_intgl)




//
//scf(0)
//clf(0)
//
//subplot(3,2,1)
//
//scatter(xi_nu(:,1),xi_nu(:,2),5)
//
//a=gca()
//a.data_bounds=[0,-3,0; 6,3,10]
//
//
//
//subplot(3,2,2)
//
//scatter(xn,yn,5)
//scatter(xy(:,1),xy(:,2),5)
//xarrows([xn(1);xn(2)],[yn(1);yn(2)],1)
//xarrows([xn(2);xn(3)],[yn(2);yn(3)],1)
//xarrows([xn(3);xn(1)],[yn(3);yn(1)],1)
//a=gca()
//a.data_bounds=[0,-3,0; 6,3,10]
//
//
//
//subplot(3,2,3)
//
//scatter3(xn,yn,5)
//scatter3(xy(:,1),xy(:,2),5)
//scatter3(xy(:,1)',xy(:,2)',z1',5)
//scatter3(xy(:,1)',xy(:,2)',z2',5)
//scatter3(xy(:,1)',xy(:,2)',z3',5)
//
//a=gca()
//a.data_bounds=[0,-3,0; 6,3,1]
//a.axes_reverse=["on" "on" "off"]
//
//
//
//
//
//subplot(3,2,4)
//scatter([x1t(1);x2t(1);x3t(1)],[x1t(2);x2t(2);x3t(2)],5)
//xarrows([x1t(1);x2t(1)],[x1t(2);x2t(2)],1)
//xarrows([x3t(1);x2t(1)],[x3t(2);x2t(2)],1)
//xarrows([x3t(1);x1t(1)],[x3t(2);x1t(2)],1)
//
//a=gca()
//a.data_bounds=[0,-1,0; 6,5,10]
//
//
//
//
//
//subplot(3,2,5)
//
//scatter3(xn,yn,5)
//scatter3(xy(:,1),xy(:,2),5)
//scatter3(xyr(:,1)',xyr(:,2)',zr',5)
//scatter3(xyr2(:,1)',xyr2(:,2)',zr2',5)
//
//
//a=gca()
//a.data_bounds=[0,-3,0; 6,3,1]
//a.axes_reverse=["on" "on" "off"]
//
