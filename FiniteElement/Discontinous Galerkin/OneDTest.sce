clear 
clc

Problem.domain.min = 0.0;
Problem.domain.max = 1.0;
Problem.q = 4.0;
Problem.D = 200.0;

//################################################ Analytical solution
Na = 100;
xa = linspace(Problem.domain.min,Problem.domain.max,Na);
ya = (1/Problem.D)*(xa-(1/2/%pi)*cos(2*%pi*xa))+1/2/%pi/Problem.D +1*0;

//################################################ Finite Difference
function [xvector, uvector]=FiniteDifference(Problem,Nel)
    L = Problem.domain.max-Problem.domain.min;
    dx= L/Nel;
    x = linspace(Problem.domain.min,Problem.domain.max,Nel+1);
    u = zeros(Nel+1,1);
    
    u(1) = 0;
    for k=2:(Nel+1)
        q = sin(2*%pi*x(k))+1;
        u(k) = u(k-1)+dx*q*0.0001/Problem.D
    end
    xvector = x;
    uvector = u;
endfunction


//################################################ LDG
//====================================== Linear shape functions
function [value]=shape_o1(index,x)
    if (index==1) then
        value = 0.5-0.5*x;
    else
        value = 0.5+0.5*x;
    end

endfunction

function [value]=shape_grad_o1(index,x,dx)
    if (index==1) then
        value = -0.5*2/dx;
    else
        value = 0.5*2/dx;
    end
endfunction


function [xvector, umatrix]=LDG(Problem,Nel)
    qpoints=[1/sqrt(3); -1/sqrt(3)];
    qweight=[1;1];
    
    L = Problem.domain.max-Problem.domain.min;
    dx= L/Nel;
    D = Problem.D
    
    x = linspace(Problem.domain.min,Problem.domain.max,Nel+1);
    umatrix = zeros(Nel,2);
    
    detJ = dx/2;
    A=zeros(2,2);
    b=zeros(2,1);
    for k=1:(Nel);
        for i=1:2 do
            for j=1:2 do
                A(i,j) =        - qweight(1)*shape_grad_o1(i,qpoints(1),dx)*D*shape_o1(j,qpoints(1))*detJ;
                A(i,j) = A(i,j) - qweight(2)*shape_grad_o1(i,qpoints(2),dx)*D*shape_o1(j,qpoints(2))*detJ;
                
                if ((i==2) && (j==2)) then
                    A(i,j) = A(i,j)+D;
                end
            end
            x1 = (qpoints(1)+1)*dx/2 + x(k);
            x2 = (qpoints(2)+1)*dx/2 + x(k);
            q1 = sin(2*%pi*x1)+1;
            q2 = sin(2*%pi*x2)+1;
            
            b(i) =        qweight(1)*shape_o1(i,qpoints(1))*q1*detJ*0.0001
            b(i) = b(i) + qweight(2)*shape_o1(i,qpoints(2))*q2*detJ*0.0001
            
            if (k>1) then
                b(1) = b(1) + D*umatrix(k-1,2)/2;
            else
                b(1) = b(1) + D*0/2;
            end
            
        end

        ulocal = linsolve(A,-b);
        umatrix(k,1) = ulocal(1);
        umatrix(k,2) = ulocal(2);
    end
    xvector = x;
endfunction







//################################################ Plotting
scf(0)
clf(0)
//plot(xa',ya',"k")

[x,u] = FiniteDifference(Problem,100)

plot(x',u,"kd")

NDG=5
[x,umat] = LDG(Problem,NDG);

for k=1:NDG do
    plot([x(k);x(k+1)],[umat(k,1);umat(k,2)])
end


