print("Simple python program solving Poisson problem")
import numpy as np

#====================================== Problem Input
class Problem1D:
    def __init__(self):
        self.q = 2.0;
        self.xmin =-1.0;
        self.xmax = 1.0;
        
#====================================== Diffusion finite difference solver        
class DiffusionFD1D:
    def __init__(self):
        print("DiffusionFD1D created.")
        
    def solve(self, P, N):
        x = np.linspace(P.xmin,P.xmax,N+1);
        phi = np.zeros(N+1,1);
        A = np.zeros(N+1,N+1);
        
        dx = (P.xmax-P.xmin)/N;
        
        print(A([0,0]))
        
        
P=Problem1D()
P.q = 1.0;
P.xmin = 0.0;

solver = DiffusionFD1D;
solver.solve(P,20)




print(P.xmin)