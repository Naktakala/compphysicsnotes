
function [value,coeffList,powList]=Legendre_ell(ell,x)
    K = floor(ell/2);
    coeffList = zeros(K,1);
    powList   = zeros(K,1);
    for k=0:(K)
        one_over2ell = 1.0/2^ell;
        mone_k = (-1)^(k);
        twoell_twok = factorial(2*ell-2*(k));
        kfact = factorial((k));
        ell_mink = factorial(ell-(k));
        ell_min2k= factorial(ell-2*(k));
        
        coeffList(k+1) = one_over2ell*mone_k*twoell_twok/kfact/ell_mink/ell_min2k;
        powList(k+1)   = ell-2*(k);
    end
    
    if ~exists("x","local") then
        x=0;
    end
    [rows,cols]=size(coeffList);
    value=0.0;
    for k=1:rows
        value=value+coeffList(k)*(x)^(powList(k));
    end
endfunction


function [value]=Associated_Legendre_ell_m(ell,em,x)
    [value,coeffList,powList]=Legendre_ell(ell);
    
    [numCoeffs,cols]=size(coeffList);
    m=abs(em)
    for i=1:m
        for j=1:numCoeffs
            if (powList(j)==0) then
                coeffList(j)=0;
            else
                coeffList(j)=coeffList(j)*powList(j);
                powList(j)=powList(j)-1;
            end
        end
    end
    
    [rows,cols]=size(coeffList);
    value=0.0;
    for k=1:rows
        value=value+coeffList(k)*(x)^(powList(k));
    end
    
    value = value*((-1)^m)*sqrt((1-x^2)^m)
    
    if (em<0) then
        value=value*((-1)^m);
        value=value*factorial(ell-m);
        value=value/factorial(ell+m);
    end
endfunction


//function [value]=PolyByCoeff(coeffList,powList,x)
//    [rows,cols]=size(coeffList);
//    value=0.0;
//    for k=1:rows
//        value=value+coeffList(k)*(x)^(powList(k));
//    end
//endfunction
//
//PolyList = [];
//
//for ell=0:10
//    [value,Poly.coeffList,Poly.powList]=Legendre_ell(ell);
//    PolyList = [PolyList; Poly];
//end

//
//x=0.75;
//
//for ell=0:10 
//    printf("P%2d = %+f\n",ell,Legendre_ell(ell,x))
//end
//
//for ell=0:10
//    printf("L=%2d  ",ell);
//    for em=(-ell):ell
//        printf("m=%+2d, Plm=%+9f  ",em,Associated_Legendre_ell_m(ell,em,x));
//    end
//    printf("\n");
//
//end






