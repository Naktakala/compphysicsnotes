clear 
clc
NHD=100;
xHD=linspace(0,2*%pi,NHD);
yHD=sin(xHD);

//==================================================== Riemann integrate
function [value]=Integrate_Riemann_sin(N,from,to)
    dx = (to-from)/(N)
    x=linspace(0,%pi-dx,N);
    y=sin(x);
    
    value=0;
    for i=1:N
        value = value + y(i)*dx;
    end
endfunction

//==================================================== elli for plotting
function [value]=ell_i(i,x_raw,node_x)
    [rows,cols]=size(node_x);
    
    value=1.0;
    for j=1:(cols)
        if (j~=i) then
            value=value*(x_raw-node_x(j))
            value=value/(node_x(i)-node_x(j));
        end        
    end
endfunction

//=================================================== Order n poly x Order 1 poly
function [out_coeffs]=multiply_pn_p1(in_coeffs_n,in_coeffs_base)
    [rows,cols]=size(in_coeffs_n);
    out_coeffs = zeros(cols+1,1)';
    for j=2:(cols+1)
        out_coeffs(j) = in_coeffs_n(j-1)*in_coeffs_base(2);
    end
    for j=1:(cols)
        tempvalue1 = out_coeffs(j);
        tempvalue2 = in_coeffs_n(j)*in_coeffs_base(1);
        out_coeffs(j) = tempvalue1+tempvalue2;
    end
end


//===================================================== Precompute li coeffs
function [coefficients]=ell_i_precomp(i,nodes)
    [rows,cols]=size(nodes);
    temp_coeffs = [0 1];
    new_coeffs=[];
    if (i==1) then
        temp_coeffs(1)=-1*nodes(2);
        for j=3:cols
            base_coeffs = [-1*nodes(j) 1]
            new_coeffs = multiply_pn_p1(temp_coeffs,base_coeffs);
            temp_coeffs = new_coeffs;
        end
    else
        temp_coeffs(1)=-1*nodes(1);
        for j=2:cols
            if (j~=i) then
                base_coeffs = [-1*nodes(j) 1]
                new_coeffs = multiply_pn_p1(temp_coeffs,base_coeffs);
                temp_coeffs = new_coeffs;
            end
        end
    end

    //calc b_i
    temp_value=0.0;
    if (i==1) then
        temp_value=nodes(1)-nodes(2);
        for j=3:cols
            temp_value=temp_value*(nodes(1)-nodes(j))
        end
    else
        temp_value=nodes(i)-nodes(1);
        for j=2:cols
            if (j~=i) then
                temp_value=temp_value*(nodes(i)-nodes(j))
            end
        end
    end
    coefficients = new_coeffs/temp_value;
endfunction

//===================================================== Calculate p(x) given coeffs
function [value]=poly_from_coeffs(x,coeffs)
    [rows,cols]=size(coeffs);
    value=0.0;
    for k=1:cols
        value=value+coeffs(k)*x^(k-1);
    end
endfunction

//====================================================== Get plot poly 
function [p,x,y]=Lagrange_sin(N)
    x=linspace(0,%pi,N);
    y=sin(x);
    
    l_i=[]
    for i=1:N
        new_li = ell_i_precomp(i,x);
        l_i = [l_i; new_li];
    end
    
    p=zeros(NHD,1)
    for iHD=1:NHD
        for i=1:N
            //p(iHD)=p(iHD)+y(i)*ell_i(i,xHD(iHD),x);
            p(iHD)=p(iHD)+y(i)*poly_from_coeffs(xHD(iHD),l_i(i,:));
        end
    end
   
endfunction

function [intgl]=Precomputed_Integral(new_li,from,to)
    [rows,cols]=size(new_li)
    intgl = 0.0;
    for k=1:cols
        intgl = intgl + new_li(k)*to^k/k;
        intgl = intgl -new_li(k)*from^k/k;
    end
endfunction

//==================================================== Integrate using Lagrange interpolation
function [intgl]=Integrate_Lagrange_sin(N,from,to)
    x=linspace(0,%pi,N);
    y=sin(x);
    integrals=zeros(N,1);
    
    for i=1:N
        coeffs = ell_i_precomp(i,x);
        integrals(i)=Precomputed_Integral(coeffs,from,to);
    end
    disp(integrals)
    intgl=0.0;
    for i=1:N
        intgl = intgl + y(i)*integrals(i);
    end
    
endfunction








//########################################################## Outputs

printf("Hello\n")

printf("N=%3d, Riemann integral err from 0 to pi=%10.2e\n",10,Integrate_Riemann_sin(10,0,%pi)-2)
printf("N=%3d, Riemann integral err from 0 to pi=%10.2e\n",100,Integrate_Riemann_sin(100,0,%pi)-2)
printf("N=%3d, Riemann integral err from 0 to pi=%10.2e\n",200,Integrate_Riemann_sin(200,0,%pi)-2)
printf("N=%3d, Riemann integral err from 0 to pi=%10.2e\n",300,Integrate_Riemann_sin(300,0,%pi)-2)

printf("\n")
printf("N=%2d, Lagrange integral err from 0 to pi=%10.2e\n",4,Integrate_Lagrange_sin(4,0,%pi)-2)
printf("N=%2d, Lagrange integral err from 0 to pi=%10.2e\n",5,Integrate_Lagrange_sin(5,0,%pi)-2)
printf("N=%2d, Lagrange integral err from 0 to pi=%10.2e\n",6,Integrate_Lagrange_sin(6,0,%pi)-2)
printf("N=%2d, Lagrange integral err from 0 to pi=%10.2e\n",7,Integrate_Lagrange_sin(7,0,%pi)-2)

printf("\n")
printf("N=%2d, Lagrange integral err from 0 to pi=%10.2e\n",3,Integrate_Lagrange_sin(3,0,%pi)-2)

A=[1 1 1; 0 %pi/2 %pi; 0 %pi^2/4 %pi^2];
b=[%pi %pi^2/2 %pi^3/3]';

li_m = linsolve(A,-b);
disp(li_m)

scf(0)
clf(0)
subplot(221)
plot(xHD,yHD,"k")


[p,x,y]=Lagrange_sin(4)
scatter(x,y)
plot(xHD,p',"r")

a=gca()
a.data_bounds=[-0.1,-1.1;3.5,1.2]
a.title.text="n=4"

subplot(222)
plot(xHD,yHD,"k")

[p,x,y]=Lagrange_sin(5)
scatter(x,y)
plot(xHD,p',"r")

a=gca()
a.data_bounds=[-0.1,-1.1;3.5,1.2]
a.title.text="n=5"

subplot(223)
plot(xHD,yHD,"k")

[p,x,y]=Lagrange_sin(6)
scatter(x,y)
plot(xHD,p',"r")

a=gca()
a.data_bounds=[-0.1,-1.1;3.5,1.2]
a.title.text="n=6"

subplot(224)
plot(xHD,yHD,"k")

[p,x,y]=Lagrange_sin(7)
scatter(x,y)
plot(xHD,p',"r")

a=gca()
a.data_bounds=[-0.1,-1.1;3.5,1.2]
a.title.text="n=7"
