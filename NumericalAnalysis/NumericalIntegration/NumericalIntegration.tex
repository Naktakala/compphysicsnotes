\documentclass[11pt,letterpaper,titlepage]{article}

%================== Document nomenclature
\newcommand{\DOCSUBJT}{Personal Notes: }   %Put document subject here
\newcommand{\DOCTITLE}{                      %Put document title here
    Numerical Integration
}       
\newcommand{\DOCDATE} {August, 2018}         %Put document date here
\newcommand{\DOCREV}  {Rev 1.00}             %Put revision number here

%================== Misc Settings
\usepackage{fancyhdr}
\usepackage[left=0.75in, right=0.75in, bottom=1.0in]{geometry}
\usepackage{lastpage}
\usepackage{titleref}
\usepackage{booktabs}
\usepackage{appendix}

\appendixtitleon
\appendixtitletocon

\makeatletter

%================== List of figures and tables mods
\usepackage{tocloft}
\usepackage[labelfont=bf]{caption}

\renewcommand{\cftfigpresnum}{Figure\ }
\renewcommand{\cfttabpresnum}{Table\ }

\newlength{\mylenf}
\settowidth{\mylenf}{\cftfigpresnum}
\setlength{\cftfignumwidth}{\dimexpr\mylenf+1.5em}
\setlength{\cfttabnumwidth}{\dimexpr\mylenf+1.5em}



%=================== Graphics
\usepackage{graphicx}
\usepackage[breakwords]{truncate}
\usepackage{float}
\usepackage{array}
\usepackage{amsmath}
\usepackage{mdframed}
\usepackage{fancyvrb}
\usepackage{float}
\usepackage{cancel}
\usepackage{amssymb}
\graphicspath{ {images/} }
\usepackage[usenames,dvipsnames,svgnames,table]{xcolor}
\usepackage[defaultlines=2,all]{nowidow}
\usepackage{listings}
\usepackage{color}
\definecolor{Brown}{cmyk}{0,0.81,1,0.60}
\definecolor{OliveGreen}{cmyk}{0.64,0,0.95,0.40}
\definecolor{CadetBlue}{cmyk}{0.62,0.57,0.23,0}
\usepackage{pdflscape}
\usepackage{relsize}
\usepackage{verbatim}
\usepackage{tabto}
%\usepackage{upgreek}
\usepackage{enumitem}
%\usepackage{MnSymbol}% http://ctan.org/pkg/mnsymbol

%=================== Big cdot
\newcommand*\bigcdot{\mathpalette\bigcdot@{.5}}
\newcommand*\bigcdot@[2]{\mathbin{\vcenter{\hbox{\scalebox{#2}{$\m@th#1\bullet$}}}}}

%=================== Settings
\renewcommand{\baselinestretch}{1.2}
\definecolor{gray}{rgb}{0.4 0.4 0.4}
\newcommand{\stimes}{{\times}}

%================== Code syntax highlighting
\lstset{language=C++,
    frame=ltrb,
    framesep=2pt,
    basicstyle=\linespread{0.8} \small,
    keywordstyle=\ttfamily\color{OliveGreen},
    identifierstyle=\ttfamily\color{CadetBlue}\bfseries,
    commentstyle=\color{Brown},
    stringstyle=\ttfamily,
    showstringspaces=true,
    tabsize=2,}


%================== Section numbers with equation numbers
\numberwithin{equation}{section}

%================== Short \to arrow
\setlength{\medmuskip}{0mu}
%\newcommand{\tos}[1][3pt]{\mathrel{%
%   \hbox{\rule[\dimexpr\fontdimen22\textfont2-.2pt\relax]{#1}{.4pt}}%
%   \mkern-4mu\hbox{\usefont{U}{lasy}{m}{n}\symbol{41}}}}

\newcommand{\beq}{\begin{equation*}
\begin{aligned}}
\newcommand{\eeq}{\end{aligned}
\end{equation*}}

\newcommand{\beqn}{\begin{equation}
	\begin{aligned}}
\newcommand{\eeqn}{\end{aligned}
	\end{equation}}



\begin{document}
    
    \begin{titlepage}
        \pagestyle{fancy}
        \vspace*{1.0cm}
        \centering
        \vspace{1cm}
        \vspace{.25cm}
        {\Large\bfseries  \DOCSUBJT \par} 
        {\Large\bfseries \DOCTITLE  \par}
        \vspace{1cm}
        {\Large \DOCDATE \par}
        \vspace{1.0cm}
        {\Large Jan Vermaak \par}
        {\Large \DOCREV \par}
        
    \end{titlepage}	
    
    
    \pagestyle{fancy}
    \rfoot{Page \thepage \ of \pageref{LastPage}}
    \cfoot{}
    \lfoot{\truncate{14cm}{\DOCTITLE}}
    \rhead{}
    \chead{\currentname}
    \lhead{}
    \renewcommand{\footrulewidth}{0.4pt}
    
    \begin{comment}
    \tableofcontents
    \addtocontents{toc}{~\hfill\textbf{Page}\par}
    
    \listoffigures
    \listoftables
    
    \end{comment}
    \chead{Contents}	
    
    %#########################################################################
    \newpage
    \chead{Basics}
    \section{Basics - The Riemann-Sum}
    Numerical integration is basically us seeking the value of a definite integral
    \begin{align}
    \int_a^b f(x).dx
    \end{align}
    \noindent
    where we know the values of $a$ and $b$. Of course we have to ask: Why do we want to do this is we learned all these wonderful anti-integral rules in Calculus?". The answer can be found when looking at the simple function
    \begin{align*}
        f(x) = e^{-x^2}.
    \end{align*}
    \noindent This function does not have an anti-integral, in fact, it requires the use of the $erf$-error function. It is a useful function for distribution as can be seen in figure \ref{fig:expminxsq} below.
    
    \begin{figure}[h]
        \centering
        \includegraphics[width=0.35\linewidth]{exp_minxsq}
        \caption{Plot of $e^{-x^2}$.}
        \label{fig:expminxsq}
    \end{figure}

\noindent
In order to integrate this function numerically we can use the most simplest of numerical techniques known as the Riemann-summation expressed as
\begin{align}
\int_a^b f(x).dx \approx \sum_{i=1}^N f(x_i).\Delta x_i
\end{align}
\noindent and can be represented graphically as shown in figure \ref{fig:1024px-riemannsumconvergence}. This method generally increases in accuracy as the number of elements $N$ are increased (which inherently decreases $\Delta x_i$) but for a few points its accuracy is dependent on the shape of the function as well as where we place $x_i$. When we place $x_i$ halfway between an interval we effectively use the midpoint rule. We can also use other methods like the trapezoid rule, or any other more exotic method.

An example algorithm for a Riemann sum is shown below figure \ref{fig:1024px-riemannsumconvergence} with its associated output.

\begin{figure}[H]
   \centering
   \includegraphics[width=0.5\linewidth]{1024px-Riemann_sum_convergence}
   \caption{Graphical representation of a Riemann-summation.}
   \label{fig:1024px-riemannsumconvergence}
\end{figure}

\begin{lstlisting}[language=Scilab]
function [value]=Integrate_Riemann_sin(N,from,to)
    dx = (to-from)/(N)
    x=linspace(0,%pi-dx,N);
    y=sin(x);
    
    value=0;
    for i=1:N
        value = value + y(i)*dx;
    end
endfunction
\end{lstlisting}

\begin{verbatim}
N= 10, Riemann integral err from 0 to pi=-1.647646e-02
N=100, Riemann integral err from 0 to pi=-1.644961e-04
N=200, Riemann integral err from 0 to pi=-4.112352e-05
N=300, Riemann integral err from 0 to pi=-1.827708e-05
\end{verbatim}

\newpage
\section{Integration using Polynomial Interpolation}
\subsection{Lagrange interpolation}
Suppose we have nodal points at $x_1,x_2,...,x_n$ and function values at these nodes $f_1,f_2,...,f_n$. The Lagrange-polynomial, $\ell_i(x)$ for point $x_i$ is given by

\begin{align} \label{eq:LagrangePoly}
\ell_i(x) = \prod_{\substack{j=0\\ i\ne j}}^n \ \frac{x\ -\  x_j}{x_i\ -\ x_j}.
\end{align}
\newline
Notice that this polynomial assumes the value of $1$ at $x_i$ and $0$ at any $i\ne j$ and therefore, when multiplied by $f_i$, can be used to approximate a function using a polynomial of the form

\begin{align}
p(x) = \sum_{i=0}^n f_i \ell_i (x).
\end{align}
\newline
This polynomial can be used to interpolate a function $f(x)$ given its values at $x_i \in [a,b]$. An example of such an interpolation for this function $f(x)=\sin x$ is shown in figure \ref{fig:sinxlagrange} below. A prototype program is also included below it.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\linewidth]{SinXLagrange}
    \caption{Interpolation of $\sin x$ using different degrees of Lagrange Polynomials.}
    \label{fig:sinxlagrange}
\end{figure}

\begin{lstlisting}[language=Scilab]
function [value]=ell_i(i,x_raw,node_x)
    [rows,cols]=size(node_x);
    
    value=1.0;
    for j=1:(cols)
        if (j~=i) then
            value=value*(x_raw-node_x(j))
            value=value/(node_x(i)-node_x(j));
        end        
    end
endfunction
\end{lstlisting}


\begin{lstlisting}[language=Scilab]
function [p,x,y]=Lagrange_sin(N)
    x=linspace(0,2*%pi,N);
    y=sin(x);
    
    p=zeros(NHD,1)
    for iHD=1:NHD
        for i=1:N
            p(iHD)=p(iHD)+y(i)*ell_i(i,xHD(iHD),x);
        end
    end
endfunction
\end{lstlisting}

\noindent
{\color{red} \textbf{WARNING}: The above algorithm is extremely inefficient since it manually computes the value of equation \ref{eq:LagrangePoly} for every provided x. In real problems we would precompute the coefficients.}
\newline
\newline
\noindent
Using this interpolation function we can now approximate the integral of the function $f(x)$ by using its polynomial interpolation $p(x)$ as

\begin{equation} \label{eq:elliintegral}
\begin{aligned}
\int_a^b f(x) .dx &\approx \int_a^b p(x) .dx \\
&=\int_a^b \sum_{i=0}^n f_i \ell_i (x) .dx \\
&= \sum_{i=0}^n f_i \int_a^b \ell_i (x) .dx
\end{aligned}
\end{equation}
\newline
Of course in order to apply this formula in a program we need to find an algorithm to express the Lagrange polynomial $\ell_i$ as  

\begin{align*}
\ell_i(x) = \prod_{\substack{j=0\\ i\ne j}}^n \ \frac{x\ -\  x_j}{x_i\ -\ x_j} = a_0 + a_1 x + a_2 x^2 +...+a_nx^n
\end{align*}
\newline
so that we can programatically integrate it. First thing we can do is to see that the denominator of this polynomial can be precomputed

\begin{align*}
\prod_{\substack{j=0\\ i\ne j}}^n \ \frac{x\ -\  x_j}{x_i\ -\ x_j} 
&=\biggr(
\prod_{\substack{j=0\\ i\ne j}}^n \ \frac{1}{x_i\ -\ x_j}
\biggr)
\prod_{\substack{j=0\\ i\ne j}}^n  \ (x\ -\  x_j) \\
&= b_i \prod_{\substack{j=0\\ i\ne j}}^n  \ (x\ -\  x_j)
\end{align*}
\noindent
Therefore
\begin{align*}
b_i =\prod_{\substack{j=0\\ i\ne j}}^n \ \frac{1}{x_i\ -\ x_j}
\end{align*}
\noindent
And we have reduced $\ell_i(x)$ to
\begin{align*}
\ell_i(x) = b_i (c_0 + c_1 x + c_2 x^2 +...+c_nx^n)
\end{align*}
\noindent
Now we can calculate the coefficients $c_j$, but first we need an algorithm to find the coefficients $c_0, c_1, ..., c_n$ given a polynomial with coefficients $a_0,a_1,...,a_{n-1}$ multiplied by a polynomial with coefficients $b_0,b_1$. In other words, we want to calculate the coefficients of a polynomial of degree $n-1$ multiplied with a polynomial of degree $1$. In mathematical form this is
\begin{align*}
c_0 + c_1 x + c_2 x^2 +...+c_nx^n = (a_0 + a_1 x + a_2 x^2 +...+a_{n-1}x^{n-1})(b_0 + b_1 x)
\end{align*}
\noindent
Such an algorithm can look like this

\begin{lstlisting}[language=Scilab]
function [out_coeffs]=multiply_pn_p1(in_coeffs_n,in_coeffs_base)
    [rows,cols]=size(in_coeffs_n);
    out_coeffs = zeros(cols+1,1)';
    for j=2:(cols+1)
        out_coeffs(j) = in_coeffs_n(j-1)*in_coeffs_base(2);
    end
    for j=1:(cols)
        tempvalue1 = out_coeffs(j);
        tempvalue2 = in_coeffs_n(j)*in_coeffs_base(1);
        out_coeffs(j) = tempvalue1+tempvalue2;
    end
end
\end{lstlisting}
\newpage
\noindent Now we can calculate the coefficients with
\begin{lstlisting}[language=Scilab]
function [coefficients]=ell_i_precomp(i,nodes)
    [rows,cols]=size(nodes);
    temp_coeffs = [0 1];   new_coeffs=[];
    if (i==1) then
        temp_coeffs(1)=-1*nodes(2);
        for j=3:cols
            base_coeffs = [-1*nodes(j) 1]
            new_coeffs = multiply_pn_p1(temp_coeffs,base_coeffs);
            temp_coeffs = new_coeffs;
        end
    else
        temp_coeffs(1)=-1*nodes(1);
        for j=2:cols
            if (j~=i) then
                base_coeffs = [-1*nodes(j) 1]
                new_coeffs = multiply_pn_p1(temp_coeffs,base_coeffs);
                temp_coeffs = new_coeffs;
            end
        end
    end
    //calc b_i
    temp_value=0.0;
    if (i==1) then
        temp_value=nodes(1)-nodes(2);
        for j=3:cols
            temp_value=temp_value*(nodes(1)-nodes(j))
        end
    else
        temp_value=nodes(i)-nodes(1);
        for j=2:cols
            if (j~=i) then
                temp_value=temp_value*(nodes(i)-nodes(j))
            end
        end
    end
    coefficients = new_coeffs/temp_value;
endfunction
\end{lstlisting}

\newpage
\noindent
With this algorithm in hand (the one that gives us the coefficients), we can determine the integral using the algorithm

\begin{lstlisting}[language=Scilab]
function [intgl]=Integrate_Lagrange_sin(N,from,to)
    y=sin(x);
    integrals=zeros(N,1);
    
    for i=1:N
        coeffs = ell_i_precomp(i,x);
        integrals(i)=Precomputed_Integral(coeffs,from,to);
    end
    
    intgl=0.0;
    for i=1:N
        intgl = intgl + y(i)*integrals(i);
    end
endfunction
\end{lstlisting}

\noindent
We can test this algorithm using the $\sin x$ integrated from $0$ to $\pi$ for which the output is

\begin{verbatim}
N= 4, integral err from 0 to pi= 4.052428e-02
N= 5, integral err from 0 to pi=-1.429268e-03
N= 6, integral err from 0 to pi=-7.969061e-04
N= 7, integral err from 0 to pi= 1.781364e-05
\end{verbatim}

\noindent
Since we precomputed the coefficients of the Lagrange polynomials, and since $\ell_i$ is independent of the function-values at the nodes, we also pre-computed the integrals and therefore the final part of the function requires $N{\times}$ multiplications but requires much fewer points (7 in this case)to achieve an accuracy comparable to a Riemann sum with 300 points. Effectively a factor 43 increased performance.

\newpage
\subsection{Newton-Cotes formula form and method of undetermined coefficients}
The Newton-Cotes formula is given by

\begin{align} \label{eq:Newton-Cotes}
\int_a^b f(x) .dx &\approx \sum_{i=0}^n A_i f(x_i)
\end{align}
\newline
where 
\begin{align*}
A_i = \int_a^b \ell_i (x) .dx.
\end{align*}
\newline
Up to now we have been looking at a way to speed up the calculation of integrals. The precomputation of Lagrange polynomials is not trivial and is expensive if we need to recompute them and their integrals ($A_i$). Another way of determining these $A_i$ coefficients is to use the \textbf{method of Undetermined Coefficients}.
\newline
\newline
With this method, we propose that the we need to find the coefficients of equation \ref{eq:Newton-Cotes} such that it is exact for all polynomials of degree $\le \ n$. Thus for every trial polynomial of order $0,1,...,n$ we will have an equation with the unknowns $A_0,A_1,..., A_n$. 
\newline
\newline
\textbf{Example:$\int_0^\pi \sin x .dx$ with three points, $x_0=0,x_1=\frac{\pi}{2},x_2=\pi$}\newline
\newline
Using the Lagrange-polynomial interpolation method introduced earlier we can find the integral $A_i$ by integrating each respective $\ell_i$ from $0$ to $\pi$. This gives us:

\begin{equation*}
\begin{bmatrix}
A_0\\
A_1\\
A_2\\
\end{bmatrix}=
\begin{bmatrix}
0.5235988\\
2.0943951\\
0.5235988\\
\end{bmatrix}
\end{equation*}

Let us take an example of a formula that will be exact for polynomial degree $\le \ 2$ by using, as trial functions $f(x)=1$, $x$ and $x^2$ for which we get

\begin{align*}
\int_0^\pi 1 .dx = \pi &= A_0 + A_1 + A_2 \\
\int_0^\pi x .dx = \frac{\pi^2}{2} &= 0.A_0 + \frac{\pi}{2} A_1 + \pi A_2 \\
\int_0^\pi x^2 .dx = \frac{\pi^3}{3} &= 0.A_0 + \frac{\pi^2}{4} A_1 + \pi^2 A_2 \\
\end{align*}
\noindent Which can be written as a system of linear equations such that
\begin{equation*}
\begin{bmatrix}
1 & 1 & 1 \\
0 & \frac{\pi}{2} & \pi \\
0 &  \frac{\pi^2}{4} & \pi^2 
\end{bmatrix}
\begin{bmatrix}
A_0\\
A_1\\
A_2\\
\end{bmatrix}=
\begin{bmatrix}
\pi\\
\frac{\pi^2}{2}\\
\frac{\pi^3}{3}
\end{bmatrix}
\end{equation*}
\newline
Solving this system requires a linear solver. Prototype code for this is shown below:

\begin{verbatim}
A=[1 1 1; 0 %pi/2 %pi; 0 %pi^2/4 %pi^2];
b=[%pi %pi^2/2 %pi^3/3]';

li_m = linsolve(A,-b);
\end{verbatim}
\noindent
For which the output is

\begin{verbatim}
0.5235988
2.0943951
0.5235988
\end{verbatim}

\noindent
In conclusion: we don't have to calculate the Lagrange polynomials to get a numerical integration formula in the Newton-Cotes form. We merely need to find the coefficients that will allow us to integrate exactly a polynomial of degree $\le \ n$.
\vspace{3cm}
\begin{equation*}
\text{ONWARDS TO THE MAGIC}
\end{equation*}

\newpage
\section{Quadrature rules for integration}
Up to now the points $x_i$ at which we have known values of the function was of our own choosing. Quadrature rules exist for the case where we only specify the interval and these rules provide the points $x_i$, called the abscissae, and the weights $w_i$ from a theoretical point of view. The easiest of these is the interval $x \in [-1,1]$ where we have the generalized form of the quadrature rule

\begin{equation}
\int_{-1}^1 \omega (x) f(x).dx \approx \sum_{i=0}^{N-1} w_i f(x_i)
\end{equation}

\noindent
Depending on the weight function $\omega (x)$ a quadrature rule may arise from areas of mathematics that are simple to handle.

\subsection{Gauss-Legendre, with $\omega (x) = 1$}
The most common quadrature is the Gauss-Legendre quadrature rule with the weight function equal to unity. This rule is known to be exact for polynomials of order $2n-1$, where $n$ is the number of absicassae. For this case the integral is simply 

\begin{equation}
\int_{-1}^1 f(x).dx \approx \sum_{i=0}^{N-1} w_i f(x_i)
\end{equation}

\noindent and the abscissae $x_n$ are the roots of the Legendre polynomial $P_n(x)$, and the weights are

\beqn\label{eq:legendreweights}
w_n = \frac{2(1-x_n^2)}{(n+1)^2  (P_{n+1}(x_n))^2}
\eeqn
\newline
From this equation we can see we need to evaluate the values of the Legendre polynomials. For this purpose we use the recursion of Legendre polynomials stating that

\beqn
P_0(x)&=1 \\
P_1(x)&=x\\
P_{n+1}(x) &= \biggr(\frac{2n+1}{n+1}\biggr)x. P_n(x) -\biggr(\frac{n}{n+1}\biggr) P_{n-1}(x)
\eeqn

\noindent
Using the code below these functions can be evaluated.
\newpage
\begin{lstlisting}[language=python]
def Legendre(N,x):
    Pnm1 = 1;
    Pn   = x;
    Pnp1 = 0;
    
    if (N==0):
        return 1;
    
    if (N==1):
        return x;
    
    for n in range(2,N+1):
        ns=n-1
        Pnp1 = ((2*ns+1)/(ns+1))*x*Pn -(ns/(ns+1))*Pnm1;
        Pnm1 = Pn;
        Pn = Pnp1;
        
    return Pnp1 
\end{lstlisting}

The other unknown in equation \ref{eq:legendreweights} is the abscissae $x_n$ which are the roots of the Legendre polynomial equations. An algorithm for finding these roots is given by Barrerra-Figueroa \cite{roots}. This algorithm utilizes Newton's method for finding a root and therefore we need to also have a function for finding the derivate of the Legendre polynomials

\beqn
P_{n}'(x) &= \frac{nx}{x^2-1}. P_n(x) - \frac{n}{x^2-1}P_{n-1}(x)
\eeqn
\newline
The code below obtains the derivative $P_n'(x)$:
\begin{lstlisting}[language=python]
def dLegendredx(N,x):
    if (N==0):
        return 0;
    
    if (N==1):
        return 1;
    
    return (N*x/(math.pow(x,2)-1))*Legendre(N,x)- \
           (N/(math.pow(x,2)-1))*Legendre(N-1,x);
\end{lstlisting}

Finally applying the root finding equation in \cite{roots}
\beqn
x_k^{(\ell+1)} &=x_k^{(\ell)} - \frac{f(x_k^{(\ell)})}{f'(x_k^{(\ell)}) - f(x_k^{(\ell)}) \sum_{j=1}^{k-1} \frac{1}{x_k^{(\ell)} - x_j  }   }
\eeqn

We get the code as shown below
\newpage

\begin{lstlisting}[language=python]
def LegendreRoots(N,maxiters=1000,tol=1.0e-10):
    xn = np.linspace(-0.999,0.999,N);  #Initial guessed values

    print("Initial guess:")
    print(xn)

    wn = np.zeros((N));
    
    
    for k in range(0,N):
        print("Finding root %d of %d" % (k+1,N), end='')
        i=0;
        while (i<maxiters):
            xold = xn[k]
            a = Legendre(N,xold)
            b = dLegendredx(N,xold)
            c = 0;
            for j in range(0,k):
                c=c+(1/(xold-xn[j]))
            
            xnew = xold - (a/(b-a*c))
            
            res=abs(xnew-xold)
            xn[k]=xnew

            if (res<tol):
                print('tr',end='')
                break
            i=i+1
        
        wn[k] = 2*(1-xn[k]*xn[k])/(N+1)/(N+1)/ \
                Legendre(N+1,xn[k])/Legendre(N+1,xn[k])
        print(" root %f, weight=%f, test=%f" %(xn[k],wn[k],Legendre(N,xn[k])))
        
    return xn,wn;
\end{lstlisting}

With the result for $P_5(x)$:
\begin{verbatim}
Finding root 1 of 5 root1 -0.906180, weight=0.236927
Finding root 2 of 5 root1 -0.538469, weight=0.478629
Finding root 3 of 5 root1 0.000000, weight=0.568889
Finding root 4 of 5 root1 0.538469, weight=0.478629
Finding root 5 of 5 root1 0.906180, weight=0.236927
\end{verbatim}


\newpage\noindent
\textbf{Application}\newline
Suppose we have the function
\beqn 
f(x) = x^7 + 2x^2
\eeqn 

\noindent with the analytical integral

\beqn 
\int_{-1}^1 ( x^7 +2x^2 ).dx = \frac{4}{3} = 1.33\dot{3}
\eeqn 

\noindent From the above algorithms we get the roots and weights as

\begin{verbatim}
Roots:
[-0.86113631 -0.33998104  0.33998104  0.86113631]
Weights:
[0.34785485 0.65214515 0.65214515 0.34785485]
\end{verbatim}

\noindent These points are graphically shown in Figure \ref{fig:figure1} below. Note also that the quadrature rule computes the integral as

\begin{verbatim}
1.3333333333333321
\end{verbatim}

which is correct to the 15th decimal. \textbf{Truly its magical to see a complex shape such as this can be integrated with such precision with only 4 points.}

\begin{figure}[H]
\centering
\includegraphics[width=0.7\linewidth]{Figure_1}
\caption{Gauss-Legendre abscissae on a test polynomial $f(x) = x^7 + 2x^2$}
\label{fig:figure1}
\end{figure}


\newpage
\subsection{Gauss-Chebyshev quadrature rule}
The \textit{Gauss-Chebyshev} quadrature rule is used when the weighting is $w(x)=\frac{1}{\sqrt{1-x^2}}$ and therefore the integral is of the form

$$
\int_{-1}^1 \frac{f(x)}{\sqrt{1-x^2}}.dx \approx \sum_{n=1}^N w_n f(x_n).
$$

Here the abscissae $x_n$ are the roots of the Chebyshev polynomials of the second kind which have an explicit solution:
\beqn
x_n = cos\biggr(\frac{(2n-1)\pi}{2N} \biggr).
\eeqn

The quadrature weights are given by the simple relation
\beqn
w_n = \frac{\pi}{N}.
\eeqn

The code below is a simple implementation of these formulas

\begin{lstlisting}[language=python]
def ChebyshevRoots(N):
    xn = np.linspace(-1,1,N)
    wn = np.linspace(-1,1,N)
    
    for n in range(0,N):
        ns=n+1
        xn[n]=math.cos((2*ns-1)*math.pi/2/N)
        wn[n]=math.pi/N
        
        print("Finding root %d of %d, root=%f, weight=%f" %(n+1,N,xn[n],wn[n]))
        
    return xn,wn
\end{lstlisting}
With the result for $U_5(x)$:
\begin{verbatim}
Finding root 1 of 5, root=0.951057, weight=0.628319
Finding root 2 of 5, root=0.587785, weight=0.628319
Finding root 3 of 5, root=0.000000, weight=0.628319
Finding root 4 of 5, root=-0.587785, weight=0.628319
Finding root 5 of 5, root=-0.951057, weight=0.628319
\end{verbatim}

\newpage
\begin{appendices}
    \vspace{1cm}
    
\end{appendices}

\newpage
\chead{References}
\begin{thebibliography}{1}
    
    \bibitem{roots} Barrera-Figueroa V., et al. {\em Multiple root finder algorithm for Legendre and Chebyshev polynomials via Newton’s method}, Annales Mathematicae et Informaticae, volume 33, pages 3-13, 2006
    
    
\end{thebibliography}

    
    
    
    
\end{document}
