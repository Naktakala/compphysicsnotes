global MaterialStack
MaterialStack.items = [];
MaterialStack.itemCount = 0;

function [newMaterial]=MaterialCreate()
    newMaterial.atomdensity = 1.0;
    newMaterial.sigma_a = 1.0;
    newMaterial.sigma_s = 1.0;
    
    global MaterialStack
    MaterialStack.itemCount = MaterialStack.itemCount +1;
    newMaterial.handle = MaterialStack.itemCount;
    MaterialStack.items(MaterialStack.itemCount) = newMaterial;
endfunction
