
global ObjectStack;
global LogicVolumeStack;
ObjectStack.items=[];
ObjectStack.itemCount=0;

LogicVolumeStack.items=[];
LogicVolumeStack.itemCount=0;

//========================================================== Create new logic volume
function [newLogic]=LogicVolumeCreate()
    newLogic.surfaces = [];
    newLogic.senses = [];
    newLogic.surfaceCount = 0;
    
    //================================== Add to stack
    global LogicVolumeStack;
    LogicVolumeStack.itemCount = LogicVolumeStack.itemCount+1;
    newLogic.handle            = LogicVolumeStack.itemCount;
    LogicVolumeStack.items     = [LogicVolumeStack.items newLogic];
endfunction

//========================================================== Add a surface
function []=LogicVolumeAddSurface(handle,surface,sense)
    global LogicVolumeStack;
    this = LogicVolumeStack.items(handle);
    
    
    
    this.surfaceCount = this.surfaceCount+1;
    this.surfaces     = [this.surfaces surface];
    this.senses       = [this.senses sense];
    LogicVolumeStack.items(handle)=this;
    printf("Logic volume surface Count= %d\n",this.surfaceCount)
endfunction

//========================================================== Get sense of volume
function [flag]=LogicVolumeGetSense(handle,x,y,z)
    global LogicVolumeStack;
    this = LogicVolumeStack.items(handle);
    
    within = %T
    for k=1:(this.surfaceCount)
        thisSurf = this.surfaces(k);
        value = thisSurf.Sense(thisSurf.handle,x,y,z);
        
        if ((value<=-0.000001) & (this.senses(k)==%T)) then
            within=%F;
        end
        
        if ((value>0.000001) & (this.senses(k)==%F)) then
            within=%F; 
        end
    end
    
    flag = within;
 
endfunction

//========================================================== Create new object
function [newObj]=ObjectCreate(imp)
    newObj.logicVolumes = [];
    newObj.logicVolumeCount = 0;
    newObj.logicSenses = [];
    newObj.importance = imp;
    
    //================================== Add to stack
    global ObjectStack;
    ObjectStack.itemCount = ObjectStack.itemCount+1;
    newObj.handle         = ObjectStack.itemCount;
    ObjectStack.items     = [ObjectStack.items newObj];
endfunction

//========================================================== Add a logic volume
function [this]=ObjectAddLogicVolume(handle,volumeHandle,sense)
    global ObjectStack;
    this = ObjectStack.items(handle);
    
    global LogicVolumeStack;
    volume = LogicVolumeStack.items(volumeHandle)
    
    this.logicVolumeCount = this.logicVolumeCount+1;
    this.logicVolumes     = [this.logicVolumes volume];
    this.logicSenses      = [this.logicSenses sense];
    ObjectStack.items(handle)=this;
endfunction

//========================================================== Is within
function [flag]=ObjectCheckSense(handle,x,y,z)
    global ObjectStack;
    thisObj = ObjectStack.items(handle);

    within = %T
    for k=1:(thisObj.logicVolumeCount)
        thisVol = thisObj.logicVolumes(k);
        value = LogicVolumeGetSense(thisVol.handle,x,y,z)
        
        if (value~=thisObj.logicSenses(k)) then
            within=%F;
        end
        
    end
    
    flag = within;
endfunction





