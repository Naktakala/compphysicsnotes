global MeshTallyStack
MeshTallyStack.items = [];
MeshTallyStack.itemCount = 0;

function [newMeshTally]=MeshTallyCreate(meshParameters)
    newMeshTally.llcorner = meshParameters.llcorner;
    newMeshTally.urcorner = meshParameters.urcorner;
    newMeshTally.divisions = meshParameters.divisions;
    
    deltas= newMeshTally.urcorner - newMeshTally.llcorner;
    deltas(1)=deltas(1)/newMeshTally.divisions(1);
    deltas(2)=deltas(2)/newMeshTally.divisions(2);
    deltas(3)=deltas(3)/newMeshTally.divisions(3);
    
    newMeshTally.matrix=[];
    for i=1:(newMeshTally.divisions(1))
        for j=1:(newMeshTally.divisions(2))
            for k=1:(newMeshTally.divisions(3))
                upperboundx = newMeshTally.llcorner(1)+deltas(1)*i;
                upperboundy = newMeshTally.llcorner(2)+deltas(2)*j;
                upperboundz = newMeshTally.llcorner(3)+deltas(3)*k;
                
                if (i>1) then
                    lowerboundx = newMeshTally.llcorner(1)+deltas(1)*(i-1);
                else
                    lowerboundx = newMeshTally.llcorner(1);
                end
                
                if (j>1) then
                    lowerboundy = newMeshTally.llcorner(2)+deltas(2)*(j-1);
                else
                    lowerboundy = newMeshTally.llcorner(2);
                end
                
                if (k>1) then
                    lowerboundz = newMeshTally.llcorner(3)+deltas(3)*(k-1);
                else
                    lowerboundz = newMeshTally.llcorner(3);
                end
                
                
                newMeshTally.matrix = [newMeshTally.matrix; 
                                       upperboundx upperboundy upperboundz lowerboundx lowerboundy lowerboundz];
            end
        end
    end
    
    [totRows, totCols]=size(newMeshTally.matrix);
    newMeshTally.values = zeros(totRows,2);
    
    global MeshTallyStack
    MeshTallyStack.itemCount = MeshTallyStack.itemCount+1;
    newMeshTally.handle = MeshTallyStack.itemCount;
    MeshTallyStack.items = [MeshTallyStack.items newMeshTally];
    
endfunction

function []=MeshTallyAddTrack(handle,pi,pf)
    global MeshTallyStack
    curTally = MeshTallyStack.items(handle);
    
    posi = [pi.x pi.y pi.z];
    posf = [pf.x pf.y pf.z];
    
    //====================================================== Find indices of start and end points
    ijk_i = [0 0 0];
    ijk_f = [0 0 0];
    
    ijk_i_found = %F;
    ijk_f_found = %F;
    
    [totRows,totCols]=size(curTally.matrix);
    for i=1:(curTally.divisions(1))
        for j=1:(curTally.divisions(2))
            for k=1:(curTally.divisions(3))
                matrixindex_0 = k + (j-1)*curTally.divisions(3) + (i-1)*curTally.divisions(3)*curTally.divisions(2);
                upperboundx = curTally.matrix(matrixindex_0,1);
                upperboundy = curTally.matrix(matrixindex_0,2);
                upperboundz = curTally.matrix(matrixindex_0,3);
                
                lowerboundx = curTally.matrix(matrixindex_0,4);
                lowerboundy = curTally.matrix(matrixindex_0,5);
                lowerboundz = curTally.matrix(matrixindex_0,6);
                
                inboundsx_i = ((pi.x<=upperboundx) & (pi.x>=lowerboundx));
                inboundsy_i = ((pi.y<=upperboundy) & (pi.y>=lowerboundy));
                inboundsz_i = ((pi.z<=upperboundz) & (pi.z>=lowerboundz));
                
                inboundsx_f = ((pf.x<=upperboundx) & (pf.x>=lowerboundx));
                inboundsy_f = ((pf.y<=upperboundy) & (pf.y>=lowerboundy));
                inboundsz_f = ((pf.z<=upperboundz) & (pf.z>=lowerboundz));
                
                if (inboundsx_i & inboundsy_i & inboundsz_i) then
                    ijk_i_found = %T;
                    ijk_i = [i j k];
                end
                
                if (inboundsx_f & inboundsy_f & inboundsz_f) then
                    ijk_f_found = %T;
                    ijk_f = [i j k];
                end
                
                if (ijk_i_found & ijk_f_found) then
                    break;
                end
                
            end
            if (ijk_i_found & ijk_f_found) then
                break;
            end
        end
        if (ijk_i_found & ijk_f_found) then
            break;
        end
    end
endfunction

//This function determines the length of a line
//drawn through a cube.
//Given the starting point it determines points of intersection
//with each of the six planes of a cube.
//We will only accept t values [0,1]
function [tracklength]=MeshTallyIntersectCube(p0,p1,bounds)
    omg = p1-p0; omg=omg/norm(omg);
    pos = p0;
    intNum = 0;
    intersections = [-1;-1];
    
    abc = [1 0 0];   //X+plane
    d = bounds.xL;
    tvalue = -99999;
    flag=%F;
    if (abs(abc*omg')>0.0000001) then
        tvalue = (d - abc*pos')/(abc*omg');
        //disp(tvalue)
        if ((tvalue>0) & (tvalue<=1)) then
            intP = p0+(tvalue)*omg;    //Intersection Point
            boundx = ((intP(1)<=(bounds.xU+0.00001)) && (intP(1)>=(bounds.xL-0.00001)));
            boundy = ((intP(2)<=(bounds.yU+0.00001)) && (intP(2)>=(bounds.yL-0.00001)));
            boundz = ((intP(3)<=(bounds.zU+0.00001)) && (intP(3)>=(bounds.zL-0.00001)));
            
            if (boundx & boundy & boundz) then
                flag=%T;
            end
        end
    end
    if (flag) then
        deltaIntersect=abs(intersections(1)-tvalue);
        if (deltaIntersect>0.000001) then
            intNum = intNum+1;
            intersections(intNum) = tvalue;
        end
    end
    
    abc = [1 0 0];   //X-plane
    d = bounds.xU;
    tvalue = -99999;
    flag=%F;
    if (abs(abc*omg')>0.0000001) then
        tvalue = (d - abc*pos')/(abc*omg');
        if ((tvalue>0) & (tvalue<=1)) then
            intP = p0+(tvalue-0.00001)*omg;    //Intersection Point
            boundx = ((intP(1)<=(bounds.xU+0.00001)) && (intP(1)>=(bounds.xL-0.00001)));
            boundy = ((intP(2)<=(bounds.yU+0.00001)) && (intP(2)>=(bounds.yL-0.00001)));
            boundz = ((intP(3)<=(bounds.zU+0.00001)) && (intP(3)>=(bounds.zL-0.00001)));
            
            if (boundx & boundy & boundz) then
                flag=%T;
            end
        end
    end
    if (flag) then
        deltaIntersect=abs(intersections(1)-tvalue);
        if (deltaIntersect>0.000001) then
            intNum = intNum+1;
            intersections(intNum) = tvalue;
        end
    end
    
    abc = [0 1 0];   //Y+plane
    d = bounds.yL;
    tvalue = -99999;
    flag=%F;
    if (abs(abc*omg')>0.0000001) then
        tvalue = (d - abc*pos')/(abc*omg');
        //disp(tvalue)
        if ((tvalue>0) & (tvalue<=1)) then
            intP = p0+(tvalue)*omg;    //Intersection Point
            boundx = ((intP(1)<=(bounds.xU+0.00001)) && (intP(1)>=(bounds.xL-0.00001)));
            boundy = ((intP(2)<=(bounds.yU+0.00001)) && (intP(2)>=(bounds.yL-0.00001)));
            boundz = ((intP(3)<=(bounds.zU+0.00001)) && (intP(3)>=(bounds.zL-0.00001)));
            
            if (boundx & boundy & boundz) then
                flag=%T;
            end
        end
    end
    if (flag) then
        deltaIntersect=abs(intersections(1)-tvalue);
        if (deltaIntersect>0.000001) then
            intNum = intNum+1;
            intersections(intNum) = tvalue;
        end
    end
    
    abc = [0 1 0];   //Y-plane
    d = bounds.yU;
    tvalue = -99999;
    flag=%F;
    if (abs(abc*omg')>0.0000001) then
        tvalue = (d - abc*pos')/(abc*omg');
        if ((tvalue>0) & (tvalue<=1)) then
            intP = p0+(tvalue-0.00001)*omg;    //Intersection Point
            boundx = ((intP(1)<=(bounds.xU+0.00001)) && (intP(1)>=(bounds.xL-0.00001)));
            boundy = ((intP(2)<=(bounds.yU+0.00001)) && (intP(2)>=(bounds.yL-0.00001)));
            boundz = ((intP(3)<=(bounds.zU+0.00001)) && (intP(3)>=(bounds.zL-0.00001)));
            
            if (boundx & boundy & boundz) then
                flag=%T;
            end
        end
    end
    if (flag) then
        deltaIntersect=abs(intersections(1)-tvalue);
        if (deltaIntersect>0.000001) then
            intNum = intNum+1;
            intersections(intNum) = tvalue;
        end
    end
    
    abc = [0 0 1];   //Z+plane
    d = bounds.zL;
    tvalue = -99999;
    flag=%F;
    if (abs(abc*omg')>0.0000001) then
        tvalue = (d - abc*pos')/(abc*omg');
        //disp(tvalue)
        if ((tvalue>0) & (tvalue<=1)) then
            intP = p0+(tvalue)*omg;    //Intersection Point
            boundx = ((intP(1)<=(bounds.xU+0.00001)) && (intP(1)>=(bounds.xL-0.00001)));
            boundy = ((intP(2)<=(bounds.yU+0.00001)) && (intP(2)>=(bounds.yL-0.00001)));
            boundz = ((intP(3)<=(bounds.zU+0.00001)) && (intP(3)>=(bounds.zL-0.00001)));
            
            if (boundx & boundy & boundz) then
                flag=%T;
            end
        end
    end
    if (flag) then
        deltaIntersect=abs(intersections(1)-tvalue);
        if (deltaIntersect>0.000001) then
            intNum = intNum+1;
            intersections(intNum) = tvalue;
        end
    end
    
    abc = [0 0 1];   //Z-plane
    d = bounds.zU;
    tvalue = -99999;
    flag=%F;
    if (abs(abc*omg')>0.0000001) then
        tvalue = (d - abc*pos')/(abc*omg');
        if ((tvalue>0) & (tvalue<=1)) then
            intP = p0+(tvalue-0.00001)*omg;    //Intersection Point
            boundx = ((intP(1)<=(bounds.xU+0.00001)) && (intP(1)>=(bounds.xL-0.00001)));
            boundy = ((intP(2)<=(bounds.yU+0.00001)) && (intP(2)>=(bounds.yL-0.00001)));
            boundz = ((intP(3)<=(bounds.zU+0.00001)) && (intP(3)>=(bounds.zL-0.00001)));
            
            if (boundx & boundy & boundz) then
                flag=%T;
            end
        end
    end
    if (flag) then
        deltaIntersect=abs(intersections(1)-tvalue);
        if (deltaIntersect>0.000001) then
            intNum = intNum+1;
            intersections(intNum) = tvalue;
        end
    end
    
    if (intNum==1) then
        tracklength=intersections(intNum);
    else
        tracklength=abs(intersections(1)-intersections(2))
    end
    
endfunction
