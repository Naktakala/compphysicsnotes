//========================================================== Surface stack
global SurfaceStack;
SurfaceStack.items=[]
SurfaceStack.itemCount=0;

global NEGATIVE_SENSE
global POSITIVE_SENSE
global INSIDE
global NOT_INSIDE
NEGATIVE_SENSE = %F
POSITIVE_SENSE = %T
INSIDE = %T
NOT_INSIDE = %F

//========================================================== Plane X
function [surfObject]= Plane_X(d)
    //================================== Initialize members
    surfObject.d = d;

    //================================== Calculate sense Method
    function [value]=Sense(handle,x,y,z)
        global SurfaceStack;
        this = SurfaceStack.items(handle);

        value = x-this.d;
    endfunction
    surfObject.Sense = Sense;
    
    //================================== Calculate intersection Method
    function [value,flag]=Intersect(handle,x0,y0,z0,omegax,omegay,omegaz)
        global SurfaceStack;
        this = SurfaceStack.items(handle);
        
        abc = [1 0 0];
        pos = [x0 y0 z0];
        omg = [omegax omegay omegaz];
        
        if (abs(abc*omg')>0.0000001) then
            value = (this.d - abc*pos')/(abc*omg');
            flag = %T
        else
            value =0.0;
            flag = %F
        end
    endfunction
    surfObject.Intersect = Intersect;

    //================================== Add to stack
    global SurfaceStack;
    SurfaceStack.itemCount = SurfaceStack.itemCount+1;
    surfObject.handle      = SurfaceStack.itemCount;
    SurfaceStack.items     = [SurfaceStack.items surfObject];
    
endfunction

//========================================================== Plane Y
function [surfObject]= Plane_Y(d)
    //================================== Initialize members
    surfObject.d = d;

    //================================== Calculate sense Method
    function [value]=Sense(handle,x,y,z)
        global SurfaceStack;
        this = SurfaceStack.items(handle);

        value = y-this.d;
    endfunction
    surfObject.Sense = Sense;
    
    //================================== Calculate intersection Method
    function [value,flag]=Intersect(handle,x0,y0,z0,omegax,omegay,omegaz)
        global SurfaceStack;
        this = SurfaceStack.items(handle);
        
        abc = [0 1 0];
        pos = [x0 y0 z0];
        omg = [omegax omegay omegaz];
        
        if (abs(abc*omg')>0.0000001) then
            value = (this.d - abc*pos')/(abc*omg');
            flag = %T
        else
            value =0.0;
            flag = %F
        end
    endfunction
    surfObject.Intersect = Intersect;

    //================================== Add to stack
    global SurfaceStack;
    SurfaceStack.itemCount = SurfaceStack.itemCount+1;
    surfObject.handle      = SurfaceStack.itemCount;
    SurfaceStack.items     = [SurfaceStack.items surfObject];
    
endfunction

//========================================================== Plane Z
function [surfObject]= Plane_Z(d)
    //================================== Initialize members
    surfObject.d = d;

    //================================== Calculate sense Method
    function [value]=Sense(handle,x,y,z)
        global SurfaceStack;
        this = SurfaceStack.items(handle);

        value = z-this.d;
    endfunction
    surfObject.Sense = Sense;
    
    //================================== Calculate intersection Method
    function [value,flag]=Intersect(handle,x0,y0,z0,omegax,omegay,omegaz)
        global SurfaceStack;
        this = SurfaceStack.items(handle);
        
        abc = [0 0 1];
        pos = [x0 y0 z0];
        omg = [omegax omegay omegaz];
        
        if (abs(abc*omg')>0.0000001) then
            value = (this.d - abc*pos')/(abc*omg');
            flag = %T
        else
            value =0.0;
            flag = %F
        end
    endfunction
    surfObject.Intersect = Intersect;

    //================================== Add to stack
    global SurfaceStack;
    SurfaceStack.itemCount = SurfaceStack.itemCount+1;
    surfObject.handle      = SurfaceStack.itemCount;
    SurfaceStack.items     = [SurfaceStack.items surfObject];
    
endfunction

//========================================================== Plane
function [surfObject]= Plane(a,b,c,d)
    //================================== Initialize members
    surfObject.a = a;
    surfObject.b = b;
    surfObject.c = c;
    surfObject.d = d;

    //================================== Calculate sense Method
    function [value]=Sense(handle,x,y,z)
        global SurfaceStack;
        this = SurfaceStack.items(handle);

        value = this.a*x + this.b*y + this.c - this.d;
    endfunction
    surfObject.Sense = Sense;
    
    //================================== Calculate intersection Method
    function [value,flag]=Intersect(handle,x0,y0,z0,omegax,omegay,omegaz)
        global SurfaceStack;
        this = SurfaceStack.items(handle);
        
        abc = [this.a  this.b  this.c];
        pos = [x0 y0 z0];
        omg = [omegax omegay omegaz];
        
        if (abs(abc*omg')>0.0000001) then
            value = (this.d - abc*pos')/(abc*omg');
            flag = %T
        else
            value =0.0;
            flag = %F
        end
    endfunction
    surfObject.Intersect = Intersect;

    //================================== Add to stack
    global SurfaceStack;
    SurfaceStack.itemCount = SurfaceStack.itemCount+1;
    surfObject.handle      = SurfaceStack.itemCount;
    SurfaceStack.items     = [SurfaceStack.items surfObject];
    
endfunction
