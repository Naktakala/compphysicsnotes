function pf = TransportParticle(pi)
    //============================================ Get the object it belongs to
    global ObjectStack;
    curObj = ObjectStack.items(pi.curObject);
    
    //============================================ Find the distance to the next surface
    distanceToSurface =99999.0;
    for v=1:(curObj.logicVolumeCount)
        curVol      = curObj.logicVolumes(v);
        curVolSense = curObj.logicSenses(v);
        
        for s=1:(curVol.surfaceCount)
            curSurface      = curVol.surfaces(s);
            curSurfaceSense = curVol.senses(s);
            
            [t,intersects] = curSurface.Intersect(curSurface.handle,pi.x,pi.y,pi.z,pi.omega(1),pi.omega(2),pi.omega(3));
            
            if (intersects) then
                if (t>0) then                    
                    point = [pi.x pi.y pi.z] + t*pi.omega;
                    
                    inObject = ObjectCheckSense(curObj.handle,point(1),point(2),point(3));
                    
                    if ((inObject) & (t<=distanceToSurface)) then
                        distanceToSurface=t;
                    end
                end
            end
        end
    end
    
    
    //============================================ Find the material
    material.Sigma_t = 0.25;
    material.numOfIso= 1;
    material.Sigma_a = 0.01;
    material.Sigma_s = 0.24;
//    material.kernel  = [0.000000  0.000000
//                        0.975610  0.349066
//                        1.000000  0.366519];
    material.kernel  = [0.000000  0.000000
                        1.000000  %pi/8];
    
    //============================================ Find distance to interaction
    distanceToInteraction = -log(rand(1))/material.Sigma_t;
    
    if (distanceToInteraction>=distanceToSurface) then
        distanceToInteraction=distanceToSurface;
    end
    
    //============================================ Process the interaction
    pf.alive = %T;
    pf.x = 0;
    pf.y = 0;
    pf.z = 0;
    pf.omega = pi.omega;
    pf.energy = 14.0;
    pf.curObject = pi.curObject;
   
    //================================== If there is an interaction
    if (distanceToInteraction<=(distanceToSurface-0.000001)) then
        pf.x = pi.x + distanceToInteraction*pi.omega(1);
        pf.y = pi.y + distanceToInteraction*pi.omega(2);
        pf.z = pi.z + distanceToInteraction*pi.omega(3);
        
        interactionTable = [                  material.Sigma_a /material.Sigma_t 1
                            (material.Sigma_a+material.Sigma_s)/material.Sigma_t 2]
        interactionP = rand(1);
        interaction = 1;
        for i=1:2
            if (interactionP<=interactionTable(i,1)) then
                interaction = interactionTable(i,2);
                break;
            end
        end
        
        if (interaction==1) then
            pf.alive = %F;
        else
            
            pf=ScatterParticle(pf,material); //See definition below
            
        end
    //================================== Otherwise transport it to the surface
    else
        //disp("Transporting to surface")
        distanceToSurface = distanceToSurface +0.0001;
        pf.x = pi.x + distanceToSurface*pi.omega(1);
        pf.y = pi.y + distanceToSurface*pi.omega(2);
        pf.z = pi.z + distanceToSurface*pi.omega(3);
        //======================================== Get object
        global ObjectStack;
        for k=1:ObjectStack.itemCount
            theObj = ObjectStack.items(k);
            
            isinhere = ObjectCheckSense(theObj.handle,pf.x,pf.y,pf.z);
            if (isinhere) then
                pf.curObject = k;
                if (theObj.importance==0) then
                    pf.alive = %F;
                end
                
                break;
            end
            
        end
    end  
    
    //============================================ Calculate tallies
//    global MeshTallyStack
//    for k=1:(MeshTallyStack.itemCount)
//        curTally = MeshTallyStack.items(k);
//        
//        MeshTallyAddTrack(curTally.handle,pi,pf);
//    end
    
endfunction



function [pf]=ScatterParticle(pi,material)
    pf.alive = pi.alive;
    pf.x = pi.x;
    pf.y = pi.y;
    pf.z = pi.z;
    pf.omega = pi.omega;
    pf.energy = pi.energy;
    pf.curObject = pi.curObject;
    
    //============================================ Get scatter angle
    theta = interp1(material.kernel(:,1),material.kernel(:,2),rand(1))
    varphi = rand(1)*2*%pi;

    //======================================== Get original orientation
    v    = pi.omega;
    theta_v  = acos(v(3));
    varphi_v = acos(v(1)/sin(theta_v));
    
    tangent = [-1*cos(%pi/2 - varphi_v)  sin(%pi/2 - varphi_v) 0];
    tangent = tangent/norm(tangent);
    binorm  = cross(v,tangent); binorm  = binorm/norm(binorm);
    
    //======================================== Get transform
    xa = sin(theta)*cos(varphi);
    ya = sin(theta)*sin(varphi);
    
    omegastar = v*cos(theta) + xa*tangent + ya*binorm;
    
    pf.omega = omegastar/norm(omegastar);
    
    
endfunction
