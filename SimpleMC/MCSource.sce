
//========================================================== Problem source
global SourceStack
SourceStack.items = [];
SourceStack.itemCount = 0;

function [newSource]=SourceCreateRectangularSource(xyz_i, xyz_f, cosines, vector)
    newSource.type      = 1;                //1=Rectangular,2=Cylindrical
    newSource.xyz_i     =xyz_i;  //Rectangular->xmin ymin zmin
    newSource.xyz_f     =xyz_f;     //Rectangular->xmax ymax zmax
    newSource.cosines   =cosines;            //Cosine between vector and angle
    newSource.vector    =vector;           //Vector along which to sample source
     
    
    
    
    global SourceStack
    SourceStack.itemCount = SourceStack.itemCount + 1;
    newSource.handle      = SourceStack.itemCount;
    SourceStack.items     = [SourceStack.items newSource];
endfunction

function [particle]=SourceGetParticle(handle)
        global SourceStack
        this = SourceStack.items(handle);
        
        particle.alive = %T;
        particle.x = 0;
        particle.y = 0;
        particle.z = 0;
        
        particle.omega = [0 0 1];
        particle.energy = 14.0;
        particle.curObject =-1;
        
        //======================================== Get position
        xintvl = this.xyz_f(1)-this.xyz_i(1);
        yintvl = this.xyz_f(2)-this.xyz_i(2);
        zintvl = this.xyz_f(3)-this.xyz_i(3);
        
        particle.x = rand()*xintvl + this.xyz_i(1);
        particle.y = rand()*yintvl + this.xyz_i(2);
        particle.z = rand()*zintvl + this.xyz_i(3);
        
        
        if (abs(xintvl)<0.0000001) then
            x = this.xyz_i(1);
        end
        if (abs(yintvl)<0.0000001) then
            y = this.xyz_i(1);
        end
        if (abs(zintvl)<0.0000001) then
            z = this.xyz_i(1);
        end
        
        //======================================== Get scattering angles
        cosineintvl = this.cosines(2) - this.cosines(1);
        
        
        costheta = rand(1)*cosineintvl + this.cosines(1);
        theta = acos(costheta);
        varphi = rand(1)*2*%pi;
        
        if (abs(cosineintvl)<0.00000001) then
            theta = 0;
        end

        //======================================== Get original orientation
        v    = this.vector;
        theta_v  = acos(v(3));
        varphi_v = acos(v(1)/sin(theta_v));
        
        tangent = [-1*cos(%pi/2 - varphi_v)  sin(%pi/2 - varphi_v) 0];
        tangent = tangent/norm(tangent);
        binorm  = cross(v,tangent); binorm  = binorm/norm(binorm);
        
        //======================================== Get transform
        xa = sin(theta)*cos(varphi);
        ya = sin(theta)*sin(varphi);
        
        omegastar = v*costheta + xa*tangent + ya*binorm;
        
        particle.omega = omegastar/norm(omegastar);

        //======================================== Get energy
        particle.energy=14.0;
        
        //======================================== Get object
        global ObjectStack;
        for k=1:ObjectStack.itemCount
            theObj = ObjectStack.items(k);
            
            isinhere = ObjectCheckSense(theObj.handle,particle.x,particle.y,particle.z);
            if (isinhere) then
                particle.curObject = k;
                break;
            end
            
        end
        
//        global avgScatAngle
//    global avgScatters
//    avgScatAngle=avgScatAngle+theta*180/%pi;
//    avgScatters=avgScatters+1;
        
endfunction
