clc
clear

exec("MCEnergyGroups.sce")
exec("MCMaterials.sce",-1)
exec("MCParametricSurfaces.sce",-1)
exec("MCObjects.sce",-1)
exec("MCSource.sce",-1)
exec("MCTransport.sce",-1)
exec("MCTallies.sce",-1)

rand('uniform')
rand('seed',100)


//src = SourceCreateRectangularSource([-0.5 -0.5 -0.5],[0.5 0.5 0.5],[-1 1],[0 0 1]);
//src = SourceCreateRectangularSource([0.0 0.0 0.0],[0.0 0.0 0.0],[-1 1],[0 0 1]);
//src = SourceCreateRectangularSource([-24.999 -25 -25],[-24.999 25 25],[1 1],[1 0 0]);
src = SourceCreateRectangularSource([-24.999 0 0],[-24.999 0 0],[1 1],[1 0 0]);


surf1 = Plane_X(-25);
surf2 = Plane_X( 25);
surf3 = Plane_Y(-25);
surf4 = Plane_Y( 25);
surf5 = Plane_Z(-25);
surf6 = Plane_Z( 25);

surfB1 = Plane_X(-5);
surfB2 = Plane_X( 5);
surfB3 = Plane_Y(-5);
surfB4 = Plane_Y( 5);
surfB5 = Plane_Z(-5);
surfB6 = Plane_Z( 5);


vol1 = LogicVolumeCreate();
LogicVolumeAddSurface(vol1.handle,surf1,POSITIVE_SENSE);
LogicVolumeAddSurface(vol1.handle,surf2,NEGATIVE_SENSE);
LogicVolumeAddSurface(vol1.handle,surf3,POSITIVE_SENSE);
LogicVolumeAddSurface(vol1.handle,surf4,NEGATIVE_SENSE);
LogicVolumeAddSurface(vol1.handle,surf5,POSITIVE_SENSE);
LogicVolumeAddSurface(vol1.handle,surf6,NEGATIVE_SENSE);

vol2 = LogicVolumeCreate();
LogicVolumeAddSurface(vol2.handle,surfB1,POSITIVE_SENSE);
LogicVolumeAddSurface(vol2.handle,surfB2,NEGATIVE_SENSE);
LogicVolumeAddSurface(vol2.handle,surfB3,POSITIVE_SENSE);
LogicVolumeAddSurface(vol2.handle,surfB4,NEGATIVE_SENSE);
LogicVolumeAddSurface(vol2.handle,surfB5,POSITIVE_SENSE);
LogicVolumeAddSurface(vol2.handle,surfB6,NEGATIVE_SENSE);

obj1 = ObjectCreate(1);
obj1 = ObjectAddLogicVolume(obj1.handle,vol1.handle,INSIDE);

obj2 = ObjectCreate(0);
obj2 = ObjectAddLogicVolume(obj2.handle,vol1.handle,NOT_INSIDE);


meshParams.llcorner = [-25 -25 -25];
meshParams.urcorner = [ 25  25  25];
meshParams.divisions= [  5   5   5];
MeshTallyCreate(meshParams);


printf("Beginning\n")

global histories
global historyCount
histories = [];
historyCount = 0;



N=1;
function RunMC(geometry, src)
    xvector = [];
    yvector = [];
    zvector = [];
    counter = 0;
    printf("Particles ran=%7d\n",0)
    
    for k=1:N
       
        particle = SourceGetParticle(src.handle);
        
        history.name="Hello"
        history.items = [particle.x particle.y particle.z];
        
        xvector=[xvector particle.x];
        yvector=[yvector particle.y];
        zvector=[zvector particle.z];

        
        while (particle.alive)
            particle = TransportParticle(particle);
            xvector=[xvector particle.x];
            yvector=[yvector particle.y];
            zvector=[zvector particle.z];
            
            history.items = [history.items; [particle.x particle.y particle.z]];
        end
        global histories
        global historyCount
        histories = [histories; history]
        historyCount = historyCount + 1;
        counter = counter + 1;
        if (counter>=100) then
            counter=0;
            printf("Particles ran=%7d \n",k)
        end

    end

    
    printf("\n")
    scf(0)
    clf(0)
    scatter3(xvector,yvector,zvector,4.0,".")

    a=gca();
    bound=30
    a.data_bounds = [-bound,-bound,-bound;bound,bound,bound]
    a.tight_limits = "on"
    
    scf(1)
    clf(1)
    global histories
    global historyCount
    for k=1:historyCount
        curHistory = histories(k)
        [numRows,numCols]=size(curHistory.items);
//        printf("History %d\n",k);
//        for i=1:numRows
//            p = curHistory.items(i,:);
//            printf("%8.4f %8.4f %8.4f\n",p(1),p(2),p(3));
//        end
        //plot(curHistory.items(:,1),curHistory.items(:,2))
    end
    
endfunction


RunMC()
bounds.xL = 0
bounds.yL = 0
bounds.zL = 0

bounds.xU = 1
bounds.yU = 1
bounds.zU = 1

p1 = [-0.5 -0.5 -0.5]
p0 = [ 0.5 0.5 0.5]

disp(MeshTallyIntersectCube(p0,p1,bounds))
printf("srqt(3)=%f, sqrt(3)/2=%f\n",sqrt(3),sqrt(3)/2)




