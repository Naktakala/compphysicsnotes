\documentclass[11pt,letterpaper,titlepage]{article}

%================== Document nomenclature
\newcommand{\DOCSUBJT}{Personal Notes: }   %Put document subject here
\newcommand{\DOCTITLE}{                      %Put document title here
	Document Title
}       
\newcommand{\DOCDATE} {August, 2018}         %Put document date here
\newcommand{\DOCREV}  {Rev 1.00}             %Put revision number here

%================== Misc Settings
\usepackage{fancyhdr}
\usepackage[left=0.75in, right=0.75in, bottom=1.0in]{geometry}
\usepackage{lastpage}
\usepackage{titleref}
\usepackage{booktabs}
\usepackage{appendix}

\appendixtitleon
\appendixtitletocon

\makeatletter

%================== List of figures and tables mods
\usepackage{tocloft}
\usepackage[labelfont=bf]{caption}

\renewcommand{\cftfigpresnum}{Figure\ }
\renewcommand{\cfttabpresnum}{Table\ }

\newlength{\mylenf}
\settowidth{\mylenf}{\cftfigpresnum}
\setlength{\cftfignumwidth}{\dimexpr\mylenf+1.5em}
\setlength{\cfttabnumwidth}{\dimexpr\mylenf+1.5em}



%=================== Graphics
\usepackage{graphicx}
\usepackage[breakwords]{truncate}
\usepackage{float}
\usepackage{array}
\usepackage{amsmath}
\usepackage{mdframed}
\usepackage{fancyvrb}
\usepackage{float}
\usepackage{cancel}
\usepackage{amssymb}
\graphicspath{ {images/} }
\usepackage[usenames,dvipsnames,svgnames,table]{xcolor}
\usepackage[defaultlines=2,all]{nowidow}
\usepackage{listings}
\usepackage{color}
\definecolor{Brown}{cmyk}{0,0.81,1,0.60}
\definecolor{OliveGreen}{cmyk}{0.64,0,0.95,0.40}
\definecolor{CadetBlue}{cmyk}{0.62,0.57,0.23,0}
\usepackage{pdflscape}
\usepackage{relsize}
\usepackage{verbatim}
\usepackage{tabto}
\usepackage{upgreek}
\usepackage{enumitem}

%=================== Settings
\renewcommand{\baselinestretch}{1.2}
\definecolor{gray}{rgb}{0.4 0.4 0.4}
\newcommand{\stimes}{{\times}}

%================== Code syntax highlighting
\lstset{language=C++,frame=ltrb,framesep=2pt,basicstyle=\linespread{0.8} \small,
	keywordstyle=\ttfamily\color{OliveGreen},
	identifierstyle=\ttfamily\color{CadetBlue}\bfseries,
	commentstyle=\color{Brown},
	stringstyle=\ttfamily,
	showstringspaces=true,
	tabsize=2,}

\begin{document}

\begin{titlepage}
	\pagestyle{fancy}
	\vspace*{1.0cm}
	\centering
	\vspace{1cm}
	\vspace{.25cm}
	{\Large\bfseries  \DOCSUBJT \par} 
	{\Large\bfseries \DOCTITLE  \par}
	\vspace{1cm}
	{\Large \DOCDATE \par}
	\vspace{1.0cm}
	{\Large Jan Vermaak \par}
	{\Large \DOCREV \par}

\end{titlepage}	


\pagestyle{fancy}
\rfoot{Page \thepage \ of \pageref{LastPage}}
\cfoot{}
\lfoot{\truncate{14cm}{\DOCTITLE}}
\rhead{}
\chead{\currentname}
\lhead{}
\renewcommand{\footrulewidth}{0.4pt}

\begin{comment}
\tableofcontents
\addtocontents{toc}{~\hfill\textbf{Page}\par}

\listoffigures
\listoftables

\end{comment}
\chead{Contents}	

%#########################################################################
\newpage
\section{Basics of iterative methods}
A basic tecnique for classifying non-Krylov subspace methods for iteratively solving the linear system $A\mathbf{x}=\mathbf{b}$ is to define the matrix $Q$ such that 

$$
Q\mathbf{x}=Q\mathbf{x} - A\mathbf{x} + \mathbf{b}.
$$

\noindent
The notion of an iterative method is introduced by approximating $\mathbf{x}$ at iteration $\ell$ as

\begin{equation*}
\begin{aligned}
Q\mathbf{x}^{\ell}&\approx Q\mathbf{x}^{\ell-1} - A\mathbf{x}^{\ell-1} + \mathbf{b} \\
&=(Q-A)\mathbf{x}^{\ell-1} + \mathbf{b}
\end{aligned}
\end{equation*}

\noindent The choice of $Q$ then introduces different methods, with the idea that $Q$ is easily inverted. For the simplest case we consider \textit{Richardson iteration} where $Q$ is the identity matrix

$$
\mathbf{x}^{\ell} = (I-A)\mathbf{x}^{\ell-1} +\mathbf{b}
$$
\newline
A slightly different choice is to choose $Q=D$, where $D$ is the diagonal of $A$, which leads to \textit{Jacobi iteration}

\begin{equation*}
\begin{aligned}
D\mathbf{x}^{\ell}&=(D-A)\mathbf{x}^{\ell-1} + \mathbf{b} \\
\mathbf{x}^{\ell}&=D^{-1}(D-A)\mathbf{x}^{\ell-1} + D^{-1}\mathbf{b} \\
\therefore\mathbf{x}^{\ell}&=D^{-1}(-L-U)\mathbf{x}^{\ell-1} + D^{-1}\mathbf{b} \\
\end{aligned}
\end{equation*}
\newline
Where $L$ and $U$ are the lower and upper triangular components of $A$ such that $A=D+L+U$. In this case the inverse of $D$ is still easily found.

\section{How do we apply this to neutron transport?}
In neutron transport we have the following:

\begin{equation*}
\begin{aligned}
L\psi^{\ell} = MSD\psi^{\ell-1} + q\\
\psi^{\ell} = L^{-1}MSD\psi^{\ell-1} + L^{-1}q\\
D\psi^{\ell} = DL^{-1}MSD\psi^{\ell-1} + DL^{-1}q\\
\phi^{\ell} = DL^{-1}MS\phi^{\ell-1} + DL^{-1}q\\
\end{aligned}
\end{equation*}

\newpage
\chead{References}
\begin{thebibliography}{1}
    
    \bibitem{blender} {\em Blender - a 3D modelling and rendering package}, Blender Online Community, Blender Foundation, Blender Institute, Amsterdam, 2018
    
    \bibitem{delaunay} Cheng et al, {\em Delaunay Mesh Generation}, Chapman \& Hall/CRC Computer \& Information Science Series, 2013
    
    
\end{thebibliography}





\end{document}