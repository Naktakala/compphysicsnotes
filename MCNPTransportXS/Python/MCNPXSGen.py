import numpy as np
import math
import matplotlib.pyplot as plt
import os
import WriteMCNP
import ReadMCNP
import subprocess
import Legendre
import WritePDT

#$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ CLASS DEF
class MXSG:
    #============================================ Default Constructor
    def __init__(self):
        maindir = ''

    #============================================ Initialization
    def Initialize(self,rootdir):
        self.maindir         = os.path.dirname(os.path.abspath(__file__))
        self.rootdir         = rootdir
        self.spec_numgrp     = 0
        self.spec_lowbin     = 0.0
        self.grpstr_numgrp   = 0
        self.grpstr_lowbin   = 0.0
        self.outMaterial     = '1001.70c -1'



    #============================================ Read spectrum file
    def ReadSpectrumFile(self, filename):
        spec = open(filename,'r')

        #------------------------------ Loop through each line
        #                               determine number of groups
        in_spec = False

        for line in spec:
            line = line.expandtabs()
            line = line.strip()
            words = line.split()
            if (len(line) > 0):
                if (line[0] != '#'):
                    if (words[0] == "BEGINSPECTRUM"):
                        in_spec = True
                    if (words[0] == "ENDSPECTRUM"):
                        in_spec = False
                    if (words[0] == "LOWERBINDOUNDARY"):
                        self.spec_lowbin = float(words[1])
                    if (in_spec and (not (words[0] == "BEGINSPECTRUM"))):
                        self.spec_numgrp = self.spec_numgrp + 1

        #------------------------------ Init array
        print('MXSG: number of groups in spectrum = '+ \
                 str(self.spec_numgrp))
        self.spectrum = np.zeros((self.spec_numgrp,4))

        #------------------------------ Read in data
        spec.seek(0,0)
        i = -1
        for line in spec:
            line = line.expandtabs()
            line = line.strip()
            words = line.split()
            if (len(line) > 0):
                if (line[0] != '#'):
                    if (words[0] == "BEGINSPECTRUM"):
                        in_spec = True
                    if (words[0] == "ENDSPECTRUM"):
                        in_spec = False
                    if (in_spec and (not (words[0] == "BEGINSPECTRUM"))):
                        i = i+1
                        self.spectrum[i, 0] = float(words[0])
                        self.spectrum[i, 1] = float(words[1])

        #------------------------------ Check if in decreasing order
        decreasingOrder = True
        if (self.spectrum[0,0] < self.spectrum[1,0]):
            decreasingOrder = False

        #------------------------------ If not reverse it
        if (decreasingOrder):
            temp = np.copy(self.spectrum)
            for i in range(0,self.spec_numgrp):
                self.spectrum[i,0] = temp[self.spec_numgrp-1-i,0]
                self.spectrum[i,1] = temp[self.spec_numgrp-1-i,1]

        #------------------------------ Compute divide by bin
        for i in range(0,self.spec_numgrp):
            lowbound = self.spec_lowbin
            if (i > 0):
                lowbound = self.spectrum[i-1,0]
            self.spectrum[i,2] = self.spectrum[i,1]/ \
                                 (self.spectrum[i,0] - lowbound)

        # ------------------------------ Compute bin center
        for i in range(0, self.spec_numgrp):
            lowbound = self.spec_lowbin
            if (i > 0):
                lowbound = self.spectrum[i - 1, 0]
            self.spectrum[i, 3] = (self.spectrum[i, 0] + lowbound) / 2.0

        #print(self.spectrum)
        spec.close()

        # ============================================ Read spectrum file
    def ReadGroupStructureFile(self, filename):
        spec = open(filename, 'r')

        # ------------------------------ Loop through each line
        #                               determine number of groups
        in_spec = False

        for line in spec:
            line = line.expandtabs()
            line = line.strip()
            words = line.split()
            if (len(line) > 0):
                if (line[0] != '#'):
                    if (words[0] == "BEGINGROUPSTRUCTURE"):
                        in_spec = True
                    if (words[0] == "ENDGROUPSTRUCTURE"):
                        in_spec = False
                    if (words[0] == "LOWERBINDOUNDARY"):
                        self.grpstr_lowbin = float(words[1])
                    if (in_spec and (not (words[0] == "BEGINGROUPSTRUCTURE"))):
                        self.grpstr_numgrp = self.grpstr_numgrp + 1

        # ------------------------------ Init array
        print('MXSG: number of groups in group structure = ' + \
              str(self.grpstr_numgrp))
        self.groupstruct = np.zeros((self.grpstr_numgrp,2))

        # ------------------------------ Read in data
        spec.seek(0, 0)
        i = -1
        for line in spec:
            line = line.expandtabs()
            line = line.strip()
            words = line.split()
            if (len(line) > 0):
                if (line[0] != '#'):
                    if (words[0] == "BEGINGROUPSTRUCTURE"):
                        in_spec = True
                    if (words[0] == "ENDGROUPSTRUCTURE"):
                        in_spec = False
                    if (in_spec and (not (words[0] == "BEGINGROUPSTRUCTURE"))):
                        i = i + 1
                        self.groupstruct[i,0] = float(words[0])

        # ------------------------------ Check if in decreasing order
        decreasingOrder = True
        if (self.groupstruct[0,0] < self.groupstruct[1,0]):
            decreasingOrder = False

        # ------------------------------ If not reverse it
        if (decreasingOrder):
            temp = np.copy(self.groupstruct)
            for i in range(0, self.grpstr_numgrp):
                self.groupstruct[i,0] = temp[self.grpstr_numgrp-1-i,0]

        # ------------------------------ Compute bin center
        for i in range(0, self.grpstr_numgrp):
            lowbound = self.grpstr_lowbin
            if (i > 0):
                lowbound = self.groupstruct[i - 1, 0]
            self.groupstruct[i, 1] = (self.groupstruct[i, 0] + lowbound)/2.0

        #print(self.groupstruct)
        spec.close()

    #====================================================== Set material
    def SetMaterial(self, inMaterial):
        self.outMaterial = inMaterial.rstrip()

    #====================================================== Execute
    def Execute(self,gprime):
        self.transmat = np.zeros((self.grpstr_numgrp,
                                  self.grpstr_numgrp,
                                  105))
        self.sigmat = np.zeros((self.grpstr_numgrp))
        self.sigmaa = np.zeros((self.grpstr_numgrp))
        self.sigmas = np.zeros((self.grpstr_numgrp))

        self.sigmatrr = np.zeros((self.grpstr_numgrp))
        self.sigmaarr = np.zeros((self.grpstr_numgrp))
        self.sigmasrr = np.zeros((self.grpstr_numgrp))

        WriteMCNP.CreateSpecInp("HelloSpec.txt",
                            self.outMaterial,
                            self.groupstruct,
                            self.grpstr_lowbin,
                            self.spectrum,
                             self.spec_lowbin)

        for gprime2 in range(0,self.grpstr_numgrp):
            WriteMCNP.CreateInp('Scatter' + '%03d' %(gprime2) + '.inp',
                                self.outMaterial,
                                gprime2,
                                self.groupstruct,
                                self.grpstr_lowbin,
                                self.spectrum,
                                self.spec_lowbin)

        WriteMCNP.CreateInp("Hello.txt",
                             self.outMaterial,
                             gprime,
                             self.groupstruct,
                             self.grpstr_lowbin,
                             self.spectrum,
                             self.spec_lowbin)
        subprocess.call(['rm','Hello_Output.*'])
        subprocess.call(['mcnp6', 'i=HelloSpec.txt name=Hello_SOutput. tasks 6'])
        ReadMCNP.ProcessSpectrum(self,"Hello_SOutput.o")

        subprocess.call(['mcnp6', 'i=Hello.txt name=Hello_Output. tasks 6'])
        ReadMCNP.ProcessTallies(self,'Scatter' + '%03d' %(gprime) + \
                                '.OUT.o',gprime)
        #print(self.sigmaa)

    #====================================================== Execute2
    def ExecuteAll(self):
        self.transmat = np.zeros((self.grpstr_numgrp,
                                  self.grpstr_numgrp,
                                  105))
        self.sigmat = np.zeros((self.grpstr_numgrp))
        self.sigmaa = np.zeros((self.grpstr_numgrp))
        self.sigmas = np.zeros((self.grpstr_numgrp))

        self.sigmatrr = np.zeros((self.grpstr_numgrp))
        self.sigmaarr = np.zeros((self.grpstr_numgrp))
        self.sigmasrr = np.zeros((self.grpstr_numgrp))

        WriteMCNP.CreateSpecInp("HelloSpec.txt",
                                self.outMaterial,
                                self.groupstruct,
                                self.grpstr_lowbin,
                                self.spectrum,
                                self.spec_lowbin)
        subprocess.call(['mcnp6', 'i=HelloSpec.txt name=Hello_SOutput. tasks 6'])
        ReadMCNP.ProcessSpectrum(self, "Hello_SOutput.o")

        for gprime2 in range(0, self.grpstr_numgrp):
            WriteMCNP.CreateInp('Scatter' + '%03d' % (gprime2) + '.inp',
                                self.outMaterial,
                                gprime2,
                                self.groupstruct,
                                self.grpstr_lowbin,
                                self.spectrum,
                                self.spec_lowbin)


        for gprime2 in range(0, self.grpstr_numgrp):
            ReadMCNP.ProcessTallies(self, 'Scatter' + '%03d' % (gprime2) + \
                               '.OUT.o', gprime2)


    #====================================================== Plot spectrum
    def PlotSpectrum(self):
        plt.loglog(self.spectrum[:,0],self.spectrum[:,2],linewidth=1)
        plt.title('Neutron spectrum')
        plt.xlabel('Energy [MeV]')
        plt.ylabel('Normalized Flux')
        plt.show()

    #====================================================== Plot xs
    def PlotTotalXS(self,figure_num=0):
        plt.figure(figure_num)
        plt.title('Total cross-section')
        plt.xlabel('Energy [MeV]')
        plt.ylabel('Cross-section [b]')
        plt.loglog(self.groupstruct[:, 1], self.sigmat,linewidth=1,label='MCNP')
        plt.loglog(self.groupstruct[:, 1], refCt, 'kd',markersize=2,label='NJOY')
        plt.legend()
        plt.show()

    def PlotScatteringXS(self, figure_num=0):
        plt.figure(figure_num)
        plt.title('Scattering cross-section')
        plt.xlabel('Energy [MeV]')
        plt.ylabel('Cross-section [b]')
        plt.loglog(self.groupstruct[:, 1], self.sigmas,linewidth=1,label='MCNP')
        plt.loglog(self.groupstruct[:, 1], refCs, 'kd',markersize=2,label='NJOY')
        plt.legend()
        plt.show()

    def PlotAbsorptionXS(self, figure_num=0):
        plt.figure(figure_num)
        plt.title('Absorption cross-section')
        plt.xlabel('Energy [MeV]')
        plt.ylabel('Cross-section [b]')
        plt.loglog(self.groupstruct[:, 1], self.sigmaa,linewidth=1,label='MCNP')
        plt.loglog(self.groupstruct[:, 1], refCa, 'kd',markersize=2,label='NJOY')
        plt.legend()
        plt.show()

    def PlotKernel(self, gprime, g, figure_num):
        plt.figure(figure_num)
        plt.title('Angular distribution scattering from group %d to group %d' \
                  %(gprime,g))
        plt.xlabel('Cosine of scattering angle')
        plt.ylabel('Probability')
        plt.plot(mucenters[0:105],self.transmat[gprime,g,0:105])

        plt.show()

    #====================================================== Compute Moment
    #This function computes a single moment integral for the
    #given gprime to g pair
    def ComputeMoment(self,ell,gprime,g):
        momentvector = np.zeros((105))
        integral = 0.0
        #print("HelloFrom Compute")
        for i in range(0,105):
            momentvector[i] = self.transmat[gprime, g, i]*Legendre.Legendre(ell,mucenters[i])
            integral = integral + momentvector[i]

        integral = integral*self.sigmas[self.grpstr_numgrp - 1 - gprime]

        # if (integral<1.0e-4):
        #     print('Integral %.6e' % (integral * self.sigmas[self.grpstr_numgrp - 1 - gprime]))
        # else:
        #     print('Integral %f' %(integral*self.sigmas[self.grpstr_numgrp-1-gprime]))
        #
        # print("Group sigmas=%f" %(self.sigmas[self.grpstr_numgrp-1-gprime]))
        # # plt.figure(10)
        # # plt.plot(mucenters[1:102],momentvector[1:102])
        # # plt.show
        return integral

    #====================================================== Writes PDT xs
    def WritePDTXS(self,filename,scat_order):
        WritePDT.WritePDTXSFormat(self,filename,scat_order)

mucenters = np.array([
-0.99999,
-0.9975,
-0.9851485,
-0.9654455,
-0.9457425,
-0.9260395,
-0.9063365,
-0.8866335,
-0.8669305,
-0.8472275,
-0.8275245,
-0.8078215,
-0.7881185,
-0.7684155,
-0.7487125,
-0.7290095,
-0.7093065,
-0.6896035,
-0.669901,
-0.6501985,
-0.6304955,
-0.6107925,
-0.5910895,
-0.5713865,
-0.5516835,
-0.5319805,
-0.5122775,
-0.4925745,
-0.4728715,
-0.4531685,
-0.4334655,
-0.4137625,
-0.3940595,
-0.3743565,
-0.3546535,
-0.3349505,
-0.3152475,
-0.2955445,
-0.2758415,
-0.2561385,
-0.2364355,
-0.2167325,
-0.1970295,
-0.1773265,
-0.1576235,
-0.1379205,
-0.1182175,
-0.0985147,
-0.0788119,
-0.0591089,
-0.03940595,
-0.019702995,
0,
0.019702995,
0.03940595,
0.0591089,
0.0788119,
0.0985147,
0.1182175,
0.1379205,
0.1576235,
0.1773265,
0.1970295,
0.2167325,
0.2364355,
0.2561385,
0.2758415,
0.2955445,
0.3152475,
0.3349505,
0.3546535,
0.3743565,
0.3940595,
0.4137625,
0.4334655,
0.4531685,
0.4728715,
0.4925745,
0.5122775,
0.5319805,
0.5516835,
0.5713865,
0.5910895,
0.6107925,
0.6304955,
0.6501985,
0.669901,
0.6896035,
0.7093065,
0.7290095,
0.7487125,
0.7684155,
0.7881185,
0.8078215,
0.8275245,
0.8472275,
0.8669305,
0.8866335,
0.9063365,
0.9260395,
0.9457425,
0.9654455,
0.9851485,
0.9975,
0.99999
])

refCt = np.array([
    1.323157      ,     1.135714         ,   1.213863        ,    1.269957         ,   1.080583     ,
    1.184236      ,     1.897786         ,   1.659708        ,   0.7910666         ,    1.12219,
    2.212775      ,     1.138798         ,   1.492508        ,    1.142611         ,   1.307204,
    1.777314      ,     2.287671         ,   1.322495        ,    2.733869         ,   2.182415,
    3.590843      ,     1.669843         ,   3.636969        ,    4.551594         ,   2.097299,
    2.939915      ,     3.562403         ,   4.009735        ,    4.319112         ,   4.483449,
    4.589995      ,     4.651817         ,   4.687159        ,    4.707258         ,   4.719531,
    4.728108      ,     4.732978         ,   4.735714        ,    4.737254         ,   4.738133,
    4.738646      ,     4.7392083059     ,  4.73942071824    ,   4.73953073496     ,  4.73972265273,
    4.73993542954 ,     4.74004115954    ,   4.74018068662   ,    4.74034767245    ,   4.74042246703,
    4.74056039612 ,     4.74080295016    ,   4.74104645977   ,    4.74128214009    ,   4.74150914763,
    4.74182085107 ,      4.7422840419    ,   4.74271907875   ,    4.74313561866    ,   4.74369271097,
    4.74446667753 ,     4.74522156614    ,   4.74598791122   ,    4.74695902345    ,   4.74828463563,
    4.74942330729 ,     4.75021846604    ,   4.75115451395   ,    4.75227317858    ,   4.75332243746,
    4.75417296959 ,     4.75513732382    ,    4.7562349201   ,    4.75732083525    ,   4.75843501273,
    4.75969200869 ,     4.76112130886    ,   4.76252908672   ,    4.76392545962    ,   4.76553639138,
    4.76734717095 ,     4.76915634198    ,   4.77098872495   ,    4.77305073595    ,   4.77541928311,
    4.77733419415 ,     4.77879261045    ,   4.78037379716   ,    4.78206628894    ,   4.78354517367,
    4.78473671907 ,     4.78604213715    ,   4.78739339197   ,    4.78854244831    ,   4.78962848883,
    4.79069428759 ,     4.79170884252    ,   4.79440171121   ,    4.79831663368    ,   4.80216061169,
    4.80577444991 ,     4.80968897816    ,   4.81343374379   ,    4.81699206102    ,   4.82106513854,
    4.82490252136 ,     4.82916818702    ,   4.83267483049   ,    4.83665985173    ,   4.84226892775,
    4.84717419586 ,     4.85234733842    ,   4.85722401355   ,    4.86060143056    ,   4.86333753642,
    4.86952329722 ,     4.87566279476    ,   4.88306324897   ,    4.89071209142    ,   4.89821566926,
    4.90759708946 ,     4.91208821036    ,   4.91812982328   ,    4.92892317854    ,   4.94232826138,
    4.9568071113  ,    4.97526846781     ,  4.98604988496    ,   4.99710753653     ,  5.02029761766,
    5.0515932398  ,     5.0723399046     ,  5.09496859898    ,    5.1182528845     ,  5.15205069413,
    5.24181855683 ,     5.31823049278    ,   5.38276432477   ,    5.49794333837    ,   5.65296788643,
    6.21561804499 ,     8.36585053854    ,   13.0614722646   ,     21.952319528    ,   41.2139875277
])

refCt = np.flip(refCt)

refCs = np.array([
    0.788551       ,     0.643672       ,    0.6746054       ,    0.6975683         ,   0.708624,
    0.8196584      ,      1.395663      ,      1.308103      ,      0.611766        ,   0.8525085,
    1.882769       ,    0.9533874       ,     1.366306       ,     1.065584         ,   1.287641,
    1.777292       ,     2.287649       ,     1.322473       ,     2.733847         ,   2.182393,
    3.590821       ,     1.669821       ,     3.636947       ,     4.551572         ,   2.097277,
    2.939893       ,     3.562381       ,     4.009713       ,     4.319093         ,   4.483434,
    4.589982       ,     4.651806       ,     4.687148       ,     4.707247         ,   4.719518,
    4.728094       ,     4.732959       ,     4.735691       ,     4.737224         ,   4.738093,
    4.738593       ,     4.7391363059   ,    4.73932471824   ,    4.73940373496     ,  4.73955365273,
    4.73973442954  ,     4.73982915954  ,     4.73995268662  ,     4.74009967245    ,   4.74015646703,
    4.74027639612  ,     4.74049895016  ,     4.74071645977  ,     4.74092614009    ,   4.74113014763,
    4.74141485107  ,      4.7418430419  ,     4.74224507875  ,     4.74263161866    ,   4.74315171097,
    4.74387967753  ,     4.74458956614  ,     4.74531491122  ,     4.74623702345    ,   4.74750063563,
    4.74858830729  ,     4.74935046604  ,     4.75024951395  ,     4.75132517858    ,   4.75233443746,
    4.75315496959  ,     4.75408632382  ,      4.7551469201  ,     4.75619783525    ,   4.75727701273,
    4.75849600869  ,     4.75988230886  ,     4.76125008672  ,     4.76260645962    ,   4.76417439138,
    4.76593917095  ,     4.76770234198  ,     4.76948872495  ,     4.77150173595    ,   4.77381728311,
    4.77568919415  ,     4.77711461045  ,     4.77866279716  ,     4.78031828894    ,   4.78176817367,
    4.78293371907  ,     4.78421313715  ,     4.78553739197  ,     4.78666344831    ,   4.78772948883,
    4.78877528759  ,     4.78976884252  ,     4.79241271121  ,     4.79625563368    ,   4.80003361169,
    4.80358344991  ,     4.80743597816  ,     4.81112074379  ,     4.81461806102    ,   4.81863213854,
    4.82241052136  ,     4.82661718702  ,     4.83006283049  ,     4.83398685173    ,   4.83953392775,
    4.84437619586  ,     4.84948233842  ,     4.85429401355  ,     4.85763643056    ,   4.86033353642,
    4.86644629722  ,     4.87251079476  ,     4.87982824897  ,     4.88739109142    ,   4.89480466926,
    4.90409308946  ,     4.90853321036  ,     4.91451382328  ,     4.92519617854    ,   4.93847526138,
    4.9528141113   ,    4.97111846781   ,    4.98180388496   ,    4.99276653653     ,  5.01575761766,
    5.0468042398   ,     5.0673969046   ,    5.08986559898   ,     5.1129808845     ,  5.14655069413,
    5.23574555683  ,     5.31169849278  ,     5.37587832477  ,     5.49046033837    ,   5.64474388643,
    6.20513104499  ,     8.34836753854  ,     13.0310822646  ,      21.899139528    ,   41.1126575277
])

refCs = np.flip(refCs)

refCa = np.array([
    7.560861e-05 ,       8.157689e-05  ,      8.678314e-05  ,        8.7842e-05 ,        8.73352e-05,
    8.109189e-05 ,       7.290793e-05  ,      6.208879e-05  ,      4.948062e-05 ,       4.029781e-05,
    3.797182e-05 ,       3.090091e-05  ,      2.478413e-05  ,      2.314236e-05 ,       2.213709e-05,
    2.213461e-05 ,       2.212942e-05  ,      2.212594e-05  ,      2.212508e-05 ,       2.212443e-05,
    2.212423e-05 ,       2.212073e-05  ,      2.211718e-05  ,      2.211708e-05 ,       2.210852e-05,
    2.205842e-05 ,       2.192152e-05  ,      2.159124e-05  ,      1.883189e-05 ,       1.540253e-05,
    1.283054e-05 ,       1.126454e-05  ,      1.089581e-05  ,      1.142725e-05 ,       1.277184e-05,
    1.516976e-05 ,       1.873406e-05  ,      2.350933e-05  ,      3.010958e-05 ,       4.009027e-05,
    5.348008e-05 ,       7.129054e-05  ,      9.507547e-05  ,      0.0001267095 ,       0.0001689667,
    0.0002001775 ,       0.0002129856  ,      0.0002283319  ,      0.0002479435 ,       0.0002668406,
    0.0002836997 ,       0.0003044607  ,      0.0003306921  ,      0.0003559105 ,       0.0003782975,
    0.000405963  ,      0.0004410689   ,     0.0004742441   ,     0.0005045271  ,      0.0005414713,
    0.0005880812 ,       0.0006324434  ,      0.0006727247  ,      0.0007219493 ,       0.0007840638,
    0.0008345869 ,       0.0008678493  ,       0.000905412  ,      0.0009482838 ,       0.0009873988,
    0.001017671  ,       0.001051005   ,      0.001087868   ,      0.001123009  ,       0.001157925,
    0.001196334  ,        0.00123885   ,      0.001279158   ,      0.001318211  ,       0.001361221,
    0.001408458  ,       0.001454222   ,      0.001499137   ,      0.001548404  ,       0.001602805,
    0.001645804  ,       0.001677429   ,       0.00171128   ,      0.001747252  ,       0.001777624,
    0.001802563  ,       0.001828115   ,      0.001855311   ,      0.001878346  ,       0.001898231,
    0.001918635  ,       0.001939168   ,       0.00198806   ,      0.002060093  ,       0.002127314,
    0.002190538  ,       0.002252901   ,      0.002313436   ,      0.002373081  ,       0.002432784,
    0.002492137  ,       0.002551663   ,      0.002612076   ,      0.002673112  ,       0.002734608,
    0.002798961  ,       0.002865142   ,      0.002929612   ,      0.002965024  ,        0.00300406,
    0.003076696  ,       0.003152837   ,      0.003234928   ,      0.003320918  ,       0.003411942,
    0.003503702  ,       0.003555686   ,      0.003615846   ,      0.003727303  ,       0.003853892,
    0.003994038  ,        0.00415058   ,      0.004245793   ,      0.004340708  ,       0.004540085,
    0.004788807  ,       0.004942984   ,      0.005103424   ,      0.005271166  ,       0.005500296,
    0.006073063  ,       0.006531966   ,      0.006886662   ,      0.007483234  ,       0.008223176,
    0.01048685   ,       0.01748261    ,      0.03038558    ,      0.05317847   ,        0.1013325
])

refCa = np.flip(refCa)


