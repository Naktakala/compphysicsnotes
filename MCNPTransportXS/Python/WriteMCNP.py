import numpy as np
import os

#$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
def CreateInp(filename,outMaterial,gprime,
              group_structure,ecutoff,
              spectrum,spec_ecutoff):

    num_grps = np.size(group_structure[:,0])
    spec_num_grps = np.size(spectrum[:,0])

    gprimestar = num_grps - gprime -1

    up_bound = group_structure[gprimestar,0]
    lo_bound = ecutoff+1.0e-16

    if (gprimestar > 0):
        lo_bound = group_structure[gprimestar-1,0]

    outf = open(filename,'w')

    outf.write('MCNP Input file\n')
    outf.write('c ################## CELLS ######################\n')
    outf.write('10 10001 0.1 -101 -999                          imp:n=1\n')
    outf.write('11     0          -999 +101                      imp:n=1\n')
    outf.write('c\n')
    outf.write('99     0          +999                           imp:n=0\n')
    outf.write('c\n')
    outf.write('c\n')
    outf.write('c\n')
    outf.write('c\n')
    outf.write('\n')
    outf.write('c ################ SURFACES #####################\n')
    outf.write('101 cx 0.00001   $Interaction sphere\n')
    outf.write('999 so 1.0     $Unit sphere\n')
    outf.write('c\n')
    outf.write('c\n')
    outf.write('c\n')
    outf.write('c\n')
    outf.write('c\n')
    outf.write('\n')
    outf.write('c ################ MATERIALS ####################\n')
    outf.write('m10001 ')

    matlinesCol = outMaterial.splitlines()
    for matlines in matlinesCol:
        if matlines == matlinesCol[0]:
            outf.write(matlines.rstrip() + '\n')
        else:
            outf.write('       ' + matlines.rstrip() + '\n')

    outf.write('c\n')
    outf.write('c\n')
    outf.write('c\n')
    outf.write('c\n')
    outf.write('c ################## SOURCE #####################\n')
    outf.write('mode n\n')
    outf.write('sdef x=-0.999999 erg=d1 vec=1 0 0 dir=1.0\n')

    outf.write('si1 H '+'%.4e ' %lo_bound)
    N=10
    dE = (up_bound - lo_bound)/N
    E = np.zeros((N))
    W = np.zeros((N))
    for e in range(0,N):
        E[e] = lo_bound+0.5*dE+e*dE
        W[e] = np.interp(E[e],spectrum[:,3],spectrum[:,2])

    for e in range(0, N):
        outf.write('%.4e ' %(E[e]+0.5*dE))
        if e==4:
            outf.write('\n         ')

    outf.write('\nsp1 0.0 ')

    for e in range(0, N):
        outf.write('%.4e ' %W[e])
        if e==4:
            outf.write('\n         ')

    outf.write('\n')

    #outf.write('si1 H 0.0 ' + str(lo_bound) + ' ' + str(up_bound) + '\n')
    #outf.write('sp1   0.0 0.0000    1.0000\n')


    outf.write('c\n')
    outf.write('c\n')
    outf.write('c\n')
    outf.write('c ################# TALLIES #####################\n')
    outf.write('c ================= Main angular tally\n')
    outf.write('F1:n 101\n')
    outf.write('c1 -0.9999999 -0.995 100i 0.995 0.9999999 1.000\n')
    outf.write('ft1 inc frv 1 0 0\n')
    outf.write('fu1 0 1 NT\n')
    #outf.write('e1 1.0e-3 2.0e-3\n')
    outf.write('c\n')
    outf.write('c\n')
    outf.write('c\n')
    outf.write('c ================= Tally of total uncollided, once collided\n')
    outf.write('c                   and multi-collided particles.\n')
    outf.write('c                   This tally should have multi-collided\n')
    outf.write('c                   as small as possible, else\n')
    outf.write('c                   the mfp combination with atomdensity and\n')
    outf.write('c                   sphere size is inappropriate.\n')
    outf.write('f11:n 101\n')
    outf.write('ft11 inc\n')
    outf.write('fu11 0 1 20\n')
    outf.write('e11 20.0\n')
    outf.write('c\n')
    outf.write('c\n')
    outf.write('c\n')
    outf.write('c\n')
    outf.write('c ================= Total reaction rates\n')
    outf.write('f4:n 10\n')
    outf.write('fm4 -1 10001 -1\n')
    outf.write('c\n')
    outf.write('c\n')
    outf.write('c ================= Total absorption rate\n')
    outf.write('f14:n 10\n')
    outf.write('fm14 -1 10001 -2\n')
    outf.write('c\n')
    outf.write('c ================= Flux\n')
    outf.write('f24:n 10\n')
    outf.write('c\n')

    j=0
    outf.write('e0    ')
    for g in range(0,num_grps):
        outf.write("%.3e " %group_structure[g,0])
        j=j+1
        if ((j==5) or (g == (num_grps-1))):
            j=0
            outf.write('\n')
            if (g < (num_grps-1)):
                outf.write('      ')
        # if ((g == (num_grps-1)) and (j != 5)):
        #     outf.write('\n')

    outf.write('c\n')
    outf.write('c\n')
    outf.write('c ################## PROBLEM ####################\n')
    outf.write('c 14400e6 takes    20 min on 144 quartz cores \n')
    outf.write('c 72000e6 takes 1hr40 min on 144 quartz cores \n')
    outf.write('c 72000e6 takes    50 min on 288 quartz cores \n')
    outf.write('nps 14400e6\n')
    outf.write('c\n')
    outf.write('c\n')
    outf.write('c\n')
    outf.write('c\n')
    outf.write('c ##################### EOF #####################\n')
    outf.write('\n')
    outf.write('\n')


    outf.close()

#$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
def CreateSpecInp(filename,outMaterial,
              group_structure,ecutoff,
              spectrum,spec_ecutoff):

    num_grps = np.size(group_structure[:,0])
    spec_num_grps = np.size(spectrum[:, 0])


    outf = open(filename,'w')

    outf.write('MCNP Input file\n')
    outf.write('c ################## CELLS ######################\n')
    outf.write('10 10001 0.1 -101 -999                          imp:n=1\n')
    outf.write('11     0          -999 +101                      imp:n=1\n')
    outf.write('c\n')
    outf.write('99     0          +999                           imp:n=0\n')
    outf.write('c\n')
    outf.write('c\n')
    outf.write('c\n')
    outf.write('c\n')
    outf.write('\n')
    outf.write('c ################ SURFACES #####################\n')
    outf.write('101 so 0.00001   $Interaction sphere\n')
    outf.write('999 so 1.0     $Unit sphere\n')
    outf.write('c\n')
    outf.write('c\n')
    outf.write('c\n')
    outf.write('c\n')
    outf.write('c\n')
    outf.write('\n')
    outf.write('c ################ MATERIALS ####################\n')
    outf.write('m10001 ')

    matlinesCol = outMaterial.splitlines()
    for matlines in matlinesCol:
        if matlines == matlinesCol[0]:
            outf.write(matlines.rstrip() + '\n')
        else:
            outf.write('       ' + matlines.rstrip() + '\n')

    outf.write('c\n')
    outf.write('c\n')
    outf.write('c\n')
    outf.write('c\n')
    outf.write('c ################## SOURCE #####################\n')
    outf.write('mode n\n')
    outf.write('sdef x=-0.9999999 erg=d1 vec=1 0 0 dir=1.0\n')

    outf.write('si1 H 0.0 ')
    j=0
    for i in range(0,spec_num_grps):
        outf.write('%.5e ' %spectrum[i,0])
        j = j + 1
        if ((j==5) or (i == (spec_num_grps-1))):
            if (i == (spec_num_grps-1)):
                outf.write('\n')
            else:
                outf.write('\n          ')
            j=0

    outf.write('sp1 0.0 ')
    j = 0
    for i in range(0, spec_num_grps):
        outf.write('%.5e ' % spectrum[i, 1])
        j = j + 1
        if ((j==5) or (i == (spec_num_grps-1))):
            if (i == (spec_num_grps-1)):
                outf.write('\n')
            else:
                outf.write('\n        ')
            j=0

    outf.write('c\n')
    outf.write('c\n')
    outf.write('c\n')
    outf.write('c ################# TALLIES #####################\n')
    outf.write('c ================= Total reaction rates\n')
    outf.write('f4:n 10\n')
    outf.write('fm4 -1 10001 -1\n')
    outf.write('c\n')
    outf.write('c\n')
    outf.write('c ================= Total absorption rate\n')
    outf.write('f14:n 10\n')
    outf.write('fm14 -1 10001 -2\n')
    outf.write('c\n')
    outf.write('c ================= Flux\n')
    outf.write('f24:n 10\n')
    outf.write('c\n')

    j=0
    outf.write('e0    ')
    for g in range(0,num_grps):
        outf.write("%.3e " %group_structure[g,0])
        j=j+1
        if ((j==5) or (g == (num_grps-1))):
            j=0
            outf.write('\n')
            if (g < (num_grps-1)):
                outf.write('      ')
        # if ((g == (num_grps-1)) and (j != 5)):
        #     outf.write('\n')

    outf.write('c\n')
    outf.write('c\n')
    outf.write('c ################## PROBLEM ####################\n')
    outf.write('nps 1000e6\n')
    outf.write('c\n')
    outf.write('c\n')
    outf.write('c\n')
    outf.write('c\n')
    outf.write('c ##################### EOF #####################\n')
    outf.write('\n')
    outf.write('\n')


    outf.close()