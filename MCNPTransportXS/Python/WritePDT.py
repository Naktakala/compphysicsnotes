import datetime
import numpy as np

def WritePDTXSFormat(xsgen,filename,L):
    now = datetime.datetime.now()

    outf = open(filename, 'w')

    outf.write('PDT Format Material Data File created ' + \
               now.strftime("%Y-%m-%d %H:%M")+'\n')
    outf.write('\n')
    outf.write('This file is a multigroup neutron ' + \
               'library generated from MCNP.\n')
    outf.write('1 temperatures, 1 densities, and ' + \
               str(xsgen.grpstr_numgrp) +' groups.\n')
    outf.write('\n')
    outf.write('3 neutron processes and 1 transfer processes.\n')
    outf.write('Scattering order ' + str(L) + '\n')
    outf.write('\n')
    outf.write('Microscopic cross sections are in units of barns.\n')
    outf.write('\n')
    outf.write('Temperatures in Kelvin:\n')
    outf.write('         296.15\n')
    outf.write('\n')
    outf.write('Densities in g/cc:\n')
    outf.write('              0\n')
    outf.write('\n')
    outf.write('Group boundaries in eV:\n')

    #============================================ Group boundaries
    j=0
    for g in range(0,xsgen.grpstr_numgrp):
        j=j+1
        gstar = xsgen.grpstr_numgrp - 1 - g
        gb = xsgen.groupstruct[gstar,0]*1e6
        bound = '%15g' % gb
        outf.write(bound)
        if ((j==5) or (g == (xsgen.grpstr_numgrp-1))):
            j=0
            outf.write('\n')
    bound = '%15g' % xsgen.grpstr_lowbin
    outf.write(bound+'\n')
    outf.write('\n')

    outf.write('T = 296.15 density = 0\n')
    outf.write('---------------------------------------------------\n')

    #============================================ Total Cross-section
    outf.write('MT 1\n')
    j = 0
    for g in range(0, xsgen.grpstr_numgrp):
        j = j + 1
        gstar = xsgen.grpstr_numgrp - 1 - g
        value = '%15g' %(xsgen.sigmat[gstar])
        outf.write(value)
        if ((j == 5) or (g == (xsgen.grpstr_numgrp - 1))):
            j = 0
            outf.write('\n')

    # ============================================ Scattering Cross-section
    outf.write('MT 2\n')
    j = 0
    for g in range(0, xsgen.grpstr_numgrp):
        j = j + 1
        gstar = xsgen.grpstr_numgrp - 1 - g
        value = '%15g' % (xsgen.sigmas[gstar])
        outf.write(value)
        if ((j == 5) or (g == (xsgen.grpstr_numgrp - 1))):
            j = 0
            outf.write('\n')
    # ============================================ Capture Cross-section
    outf.write('MT 102\n')
    j = 0
    for g in range(0, xsgen.grpstr_numgrp):
        j = j + 1
        gstar = xsgen.grpstr_numgrp - 1 - g
        value = '%15g' % (xsgen.sigmaa[gstar])
        outf.write(value)
        if ((j == 5) or (g == (xsgen.grpstr_numgrp - 1))):
            j = 0
            outf.write('\n')

    #============================================ Transfer matrix
    for moment in range(0,L+1):
        outf.write('MT 2519, Moment %d\n' %moment)
        print('Writing moment %d' % (moment))
        for sinkg in range(0, xsgen.grpstr_numgrp):
            #============================= Compute all
            fromto = np.zeros((xsgen.grpstr_numgrp))
            for gprime in range(0,xsgen.grpstr_numgrp):
                fromto[gprime] = xsgen.ComputeMoment(moment,gprime,sinkg)

            #============================= Determine first and last
            tol = 1.0e-15
            first = 0
            last = xsgen.grpstr_numgrp-1
            for gprime in range(0, xsgen.grpstr_numgrp):
                if (abs(fromto[gprime])>=tol):
                    first = gprime
                    break

            for gprime in range(xsgen.grpstr_numgrp-1,-1,-1):
                if (abs(fromto[gprime])>=tol):
                    last = gprime
                    break



            #============================= Print values
            outf.write('  Sink, first, last:%5d%5d%5d\n' %(sinkg,first,last))
            j=0
            #first = 0
            #last = xsgen.grpstr_numgrp-1
            for gprime in range(first,last+1):
                j = j + 1
                outf.write('%20g' %fromto[gprime])
                if ((j == 5) or (gprime == last)):
                    j = 0
                    outf.write('\n')

    outf.close()