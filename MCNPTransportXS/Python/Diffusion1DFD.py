import numpy as np
import matplotlib.pyplot as plt
import Legendre as lg


########################################################################################################################
# Finite Difference Method
########################################################################################################################
def Diffusion1DFD(N, L, q, LBC, RBC):
    dx = L/N
    x = np.zeros((N + 2))

    for i in range(1, N + 1):
        x[i] = dx/2 + (i - 1) * dx
    x[N + 1] = L

    A = np.zeros((N + 2, N + 2))
    b = np.zeros((N + 2, 1))
    phi = np.zeros((N + 2, 1))

    for i in range(0, N + 2):
        if i == 1:
            dximh = dx / 2
            dxiph = dx
        elif i == (N):
            dximh = dx
            dxiph = dx / 2
        else:
            dximh = dx
            dxiph = dx

        if i == 0:
            A[i, i] = 1
            b[i] = LBC
        elif i == (N + 1):
            A[i, i] = 1
            b[i] = RBC
        else:
            ae = 1 / dxiph
            aw = 1 / dximh
            A[i, i] = ae + aw
            A[i, i + 1] = -ae
            A[i, i - 1] = -aw
            b[i] = q * dx

    phi = np.linalg.solve(A, b)
    print(A)

    return x, phi
########################################################################################################################


########################################################################################################################
# Finite Elements Methods
########################################################################################################################
def ShapeN(i, x):
    value = 0
    if i == 0:
        value = 1/2 - 1/2*x
    elif i == 1:
        value = 1/2 - 1/2*x
    return value


def detJ(dx):
    return dx/2


def GradN(i, dx):
    value = 0
    if i == 0:
        value = - 1 / 2
    elif i == 1:
        value = 1 / 2
    return value/detJ(dx)


def Diffusion1DFE(N, L, q, LBC, RBC):
    dx = L/N
    x = np.zeros((N + 1))

    for i in range(0, N + 1):
        x[i] = i*dx

    A = np.zeros((N + 1, N + 1))
    b = np.zeros((N + 1, 1))
    phi = np.zeros((N + 1, 1))

    NP = 3
    qn, wn = lg.LegendreRoots(NP)

    for ir in range(0, N ):
        for i in range(0, 2):
            for j in range(0, 2):
                for qp in range(0, NP):
                    A[ir + i, ir + j] = wn[qp] * GradN(i, dx) * GradN(j, dx) * detJ(dx) + A[ir + i, ir + j]
            for qp in range(0, NP):
                b[ir + i] = wn[qp] * ShapeN(i, qn[qp]) * detJ(dx) * q + b[ir + i]

    for ir in range(0, N + 1):
        A[0,ir] = 0
        A[N,ir] = 0

    A[0,0] = 1
    b[0] = LBC
    A[N,N] = 1
    b[N] = RBC

    phi = np.linalg.solve(A, b)

    print(A)
    print(b)

    return x, phi
########################################################################################################################



N = 10
L = 1
q = 1.0
LBC = 0
RBC = 0

x, phi = Diffusion1DFD(N, L, q, LBC, RBC)
plt.plot(x, phi, label='fd')

x, phi = Diffusion1DFE(N, L, q, LBC, RBC)
plt.plot(x, phi, label='fe')


plt.legend()
plt.show()
