import numpy as np

def ProcessTallies(xsgen,filename,gprime):
    talf = open(filename,'r')
    auxlines = talf.readlines()
    talf.seek(0,0)



    marker1 = False
    marker2 = False
    marker3 = False
    marker4 = False
    marker5 = False
    marker6 = False
    skipped = False
    skippe2 = False
    normaly = 0.0
    totalrr = 0.0
    absorrr = 0.0
    rawflux = 0.0
    binnum = -1
    g = -1
    linenum = -1
    for line in talf:
        linenum = linenum + 1
        line = line.expandtabs()
        line = line.strip()
        words = line.split()
        #print(line)
        if ((len(line) > 0) ):
            if ((words[0] == "user") and \
                (words[1] == "bin") and \
                (words[2] == "1.00000E+00") and \
                (binnum   < 104)):
                #print(skipped)
                marker1 = True
                marker2 = True
                # if (not skipped):
                #     skipped = True
                # else:
                #     marker2 = True
                #     #print("Poop")
            if (marker3):
                g = g + 1
                gstar = xsgen.grpstr_numgrp-1 -g
                xsgen.transmat[gprime,gstar,binnum] = \
                 float(words[1])

                # print("Extracted gprime=%4d" %gprime, end='')
                # print(" g=%4d" %g, end='')
                # print(" mub=%d" %binnum, end='')
                # print(" val=%.5e" %xsgen.transmat[gprime,g,binnum])
                if (g == (xsgen.grpstr_numgrp-1)):
                    marker3 = False
                    marker2 = False
                    g=-1
                    #break
            if ((words[0] == "energy") and (marker2)):
                marker3 = True
                binnum = binnum + 1
                if (binnum == 104):
                    marker4 = True

            if ((words[0] == "user") and \
                (words[1] == "bin") and \
                (words[2] == "1.00000E+00") and \
                (marker4)):
                # if (not skippe2):
                #     skippe2 = True
                # else:
                wordsaux = auxlines[linenum+2].split()
                normaly = float(wordsaux[1])
                #print("Normalization=%f" %normaly)

            if ((words[0] == "1tally") and \
                (words[1] == "4")):
                wordsaux = auxlines[linenum+11+xsgen.grpstr_numgrp -1 \
                                    - gprime].split()
                totalrr = float(wordsaux[1])

            if ((words[0] == "1tally") and \
                (words[1] == "14")):
                wordsaux = auxlines[linenum+11+xsgen.grpstr_numgrp -1 \
                                    - gprime].split()
                absorrr = float(wordsaux[1])

            if ((words[0] == "1tally") and \
                (words[1] == "24")):
                wordsaux = auxlines[linenum+10+xsgen.grpstr_numgrp -1 \
                                    - gprime].split()
                rawflux = float(wordsaux[1])

    xsgen.transmat[gprime, gstar, :] = xsgen.transmat[gprime,gstar,:]

    summer = 0.0
    for i in range(0,xsgen.grpstr_numgrp):
        xsgen.transmat[gprime, i, :] = xsgen.transmat[gprime, i, :]/normaly
        summer = summer + np.sum(xsgen.transmat[gprime,i,:])
    # print("Sum:")
    # print(summer)
    #
    # print("Total RR tally for group gprime=%.5e" %totalrr)
    # print("Absor RR tally for group gprime=%.5e" %absorrr)
    # print("Raw flux tally for group gprime=%.5e" %rawflux)
    sigmat_gprime = totalrr/rawflux/0.1
    sigmaa_gprime = absorrr/rawflux/0.1
    sigmas_gprime = sigmat_gprime - sigmaa_gprime

    gstar = xsgen.grpstr_numgrp - 1 - gprime
    # xsgen.sigmat[gstar] = sigmat_gprime
    # xsgen.sigmaa[gstar] = sigmaa_gprime
    # xsgen.sigmas[gstar] = sigmas_gprime

    print('Group %3d ' %(gprime),end='')
    print('sig_t=%9.4f ' % xsgen.sigmat[gstar],end='')
    print('sig_s=%9.4f ' % xsgen.sigmas[gstar], end='')
    print('sig_a=%14.9f ' % xsgen.sigmaa[gstar], end='')
    print('sum=%9.4f ' % summer)

    # print("sigmat=%f" %sigmat_gprime)
    # print("sigmaa=%f" %sigmaa_gprime)
    # print("sigmas=%f" %sigmas_gprime)
    talf.close()






def ProcessSpectrum(xsgen,filename):
    talf = open(filename,'r')
    auxlines = talf.readlines()
    talf.seek(0,0)


    linenum = -1
    for line in talf:
        linenum = linenum + 1
        line = line.expandtabs()
        line = line.strip()
        words = line.split()
        #print(line)
        if ((len(line) > 0) ):

            if ((words[0] == "1tally") and \
                (words[1] == "4")):
                for g in range(0,xsgen.grpstr_numgrp):
                    wordsaux = auxlines[linenum+11+g].split()
                    xsgen.sigmatrr[g] = float(wordsaux[1])

            if ((words[0] == "1tally") and \
                (words[1] == "14")):
                for g in range(0,xsgen.grpstr_numgrp):
                    wordsaux = auxlines[linenum+11+g].split()
                    xsgen.sigmaarr[g] = float(wordsaux[1])

            if ((words[0] == "1tally") and \
                (words[1] == "24")):
                for g in range(0,xsgen.grpstr_numgrp):
                    wordsaux = auxlines[linenum+10+g].split()
                    xsgen.sigmasrr[g] = float(wordsaux[1])

    talf.close()

    xsgen.sigmat = xsgen.sigmatrr/xsgen.sigmasrr/0.1
    xsgen.sigmaa = xsgen.sigmaarr / xsgen.sigmasrr / 0.1
    xsgen.sigmas = xsgen.sigmat - xsgen.sigmaa

