import sys
sys.path.insert(0, "/Users/janv4/Desktop/Personal/"+\
                   "compphysicsnotes/MCNPTransportXS/Python")
import MCNPXSGen as xsgen
import os

generator = xsgen.MXSG();
generator.Initialize(os.getcwd());
generator.ReadSpectrumFile(generator.maindir+ \
                           "/PredefSpectrums/Test349.txt")

generator.ReadGroupStructureFile(generator.maindir+ \
                           "/PredefGrpStructs/G241_CERT.txt")

material = \
"6000.80c -1\n"

generator.SetMaterial(material)

gprime = 0
g      = 0
#generator.Execute(gprime)
generator.ExecuteAll()

generator.ComputeMoment(0,gprime,g)

generator.WritePDTXS('PDT_12000.data',3)


#generator.PlotSpectrum()
#
# generator.PlotTotalXS(1)
# generator.PlotScatteringXS(2)
# generator.PlotAbsorptionXS(3)
#generator.PlotKernel(gprime,g,4)
#generator.ComputeMoment(0,143,144)




print('Current working directory:\n' + str(generator.maindir))




print('Hello ReadGroupStructureFile')