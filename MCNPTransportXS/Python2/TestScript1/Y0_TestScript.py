import sys
sys.path.insert(0, "/Users/janv4/Desktop/Personal/"+\
                   "compphysicsnotes/MCNPTransportXS/Python2")

import MCTXS
import os

XSgen = MCTXS.MCTXS()

XSgen.SetRootDir(os.path.dirname(os.path.abspath(__file__)))

XSgen.ReadSpectrumFile(XSgen.SPEC349)
XSgen.ReadGroupStructureFile(XSgen.GRPSTRUCT145)

XSgen.SetMaterial('12000.80c -1')

XSgen.WriteMCNPInput("Hello.inp",45)

#XSgen.PlotSpectrum()