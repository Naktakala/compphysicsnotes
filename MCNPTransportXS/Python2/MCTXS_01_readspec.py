import numpy as np
import MCTXS_02_writemcnp

class Methods(MCTXS_02_writemcnp.Methods):
  # ============================================ Read spectrum file
  def ReadSpectrumFile(self, filename):
    spec = open(filename, 'r')

    # ------------------------------ Loop through each line
    #                               determine number of groups
    in_spec = False

    for line in spec:
      line = line.expandtabs()
      line = line.strip()
      words = line.split()
      if (len(line) > 0):
        if (line[0] != '#'):
          if (words[0] == "BEGINSPECTRUM"):
            in_spec = True
          if (words[0] == "ENDSPECTRUM"):
            in_spec = False
          if (words[0] == "LOWERBINDOUNDARY"):
            self.spec_lowbin = float(words[1])
          if (in_spec and (not (words[0] == "BEGINSPECTRUM"))):
            self.spec_numgrp = self.spec_numgrp + 1

    # ------------------------------ Init array
    print('MXSG: number of groups in spectrum = ' + \
          str(self.spec_numgrp))
    self.spectrum = np.zeros((self.spec_numgrp, 4))

    # ------------------------------ Read in data
    spec.seek(0, 0)
    i = -1
    for line in spec:
      line = line.expandtabs()
      line = line.strip()
      words = line.split()
      if (len(line) > 0):
        if (line[0] != '#'):
          if (words[0] == "BEGINSPECTRUM"):
            in_spec = True
          if (words[0] == "ENDSPECTRUM"):
            in_spec = False
          if (in_spec and (not (words[0] == "BEGINSPECTRUM"))):
            i = i + 1
            self.spectrum[i, 0] = float(words[0])
            self.spectrum[i, 1] = float(words[1])

    # ------------------------------ Check if in decreasing order
    decreasingOrder = True
    if (self.spectrum[0, 0] < self.spectrum[1, 0]):
      decreasingOrder = False

    # ------------------------------ If not reverse it
    if (decreasingOrder):
      temp = np.copy(self.spectrum)
      for i in range(0, self.spec_numgrp):
        self.spectrum[i, 0] = temp[self.spec_numgrp - 1 - i, 0]
        self.spectrum[i, 1] = temp[self.spec_numgrp - 1 - i, 1]

    # ------------------------------ Compute divide by bin
    for i in range(0, self.spec_numgrp):
      lowbound = self.spec_lowbin
      if (i > 0):
        lowbound = self.spectrum[i - 1, 0]
      self.spectrum[i, 2] = self.spectrum[i, 1] / \
                            (self.spectrum[i, 0] - lowbound)

    # ------------------------------ Compute bin center
    for i in range(0, self.spec_numgrp):
      lowbound = self.spec_lowbin
      if (i > 0):
        lowbound = self.spectrum[i - 1, 0]
      self.spectrum[i, 3] = (self.spectrum[i, 0] + lowbound) / 2.0

    # print(self.spectrum)
    spec.close()

    # ============================================ Read spectrum file

  def ReadGroupStructureFile(self, filename):
    spec = open(filename, 'r')

    # ------------------------------ Loop through each line
    #                               determine number of groups
    in_spec = False

    for line in spec:
      line = line.expandtabs()
      line = line.strip()
      words = line.split()
      if (len(line) > 0):
        if (line[0] != '#'):
          if (words[0] == "BEGINGROUPSTRUCTURE"):
            in_spec = True
          if (words[0] == "ENDGROUPSTRUCTURE"):
            in_spec = False
          if (words[0] == "LOWERBINDOUNDARY"):
            self.grpstr_lowbin = float(words[1])
          if (in_spec and (not (words[0] == "BEGINGROUPSTRUCTURE"))):
            self.grpstr_numgrp = self.grpstr_numgrp + 1

    # ------------------------------ Init array
    print('MXSG: number of groups in group structure = ' + \
          str(self.grpstr_numgrp))
    self.groupstruct = np.zeros((self.grpstr_numgrp, 2))

    # ------------------------------ Read in data
    spec.seek(0, 0)
    i = -1
    for line in spec:
      line = line.expandtabs()
      line = line.strip()
      words = line.split()
      if (len(line) > 0):
        if (line[0] != '#'):
          if (words[0] == "BEGINGROUPSTRUCTURE"):
            in_spec = True
          if (words[0] == "ENDGROUPSTRUCTURE"):
            in_spec = False
          if (in_spec and (not (words[0] == "BEGINGROUPSTRUCTURE"))):
            i = i + 1
            self.groupstruct[i, 0] = float(words[0])

    # ------------------------------ Check if in decreasing order
    decreasingOrder = True
    if (self.groupstruct[0, 0] < self.groupstruct[1, 0]):
      decreasingOrder = False

    # ------------------------------ If not reverse it
    if (decreasingOrder):
      temp = np.copy(self.groupstruct)
      for i in range(0, self.grpstr_numgrp):
        self.groupstruct[i, 0] = temp[self.grpstr_numgrp - 1 - i, 0]

    # ------------------------------ Compute bin center
    for i in range(0, self.grpstr_numgrp):
      lowbound = self.grpstr_lowbin
      if (i > 0):
        lowbound = self.groupstruct[i - 1, 0]
      self.groupstruct[i, 1] = (self.groupstruct[i, 0] + lowbound) / 2.0

    # print(self.groupstruct)
    spec.close()