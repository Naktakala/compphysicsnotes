\documentclass[11pt,letterpaper,titlepage]{article}

%================== Document nomenclature
\newcommand{\DOCSUBJT}{Technical Report: }   %Put document subject here
\newcommand{\DOCTITLE}{                      %Put document title here
	Introduction to diffusion problems
}       
\newcommand{\DOCDATE} {August, 2018}         %Put document date here
\newcommand{\DOCREV}  {Rev 1.00}             %Put revision number here

%================== Misc Settings
\usepackage{fancyhdr}
\usepackage[left=0.75in, right=0.75in, bottom=1.0in]{geometry}
\usepackage{lastpage}
\usepackage{titleref}
\usepackage{booktabs}
\usepackage{appendix}

\appendixtitleon
\appendixtitletocon

\makeatletter

%================== List of figures and tables mods
\usepackage{tocloft}
\usepackage[labelfont=bf]{caption}

\renewcommand{\cftfigpresnum}{Figure\ }
\renewcommand{\cfttabpresnum}{Table\ }

\newlength{\mylenf}
\settowidth{\mylenf}{\cftfigpresnum}
\setlength{\cftfignumwidth}{\dimexpr\mylenf+1.5em}
\setlength{\cfttabnumwidth}{\dimexpr\mylenf+1.5em}



%=================== Graphics
\usepackage{graphicx}
\usepackage[breakwords]{truncate}
\usepackage{float}
\usepackage{array}
\usepackage{amsmath}
\usepackage{mdframed}
\usepackage{fancyvrb}
\usepackage{float}
\usepackage{cancel}
\usepackage{amssymb}
\graphicspath{ {images/} }
\usepackage[usenames,dvipsnames,svgnames,table]{xcolor}
\usepackage[defaultlines=2,all]{nowidow}
\usepackage{listings}
\usepackage{color}
\definecolor{Brown}{cmyk}{0,0.81,1,0.60}
\definecolor{OliveGreen}{cmyk}{0.64,0,0.95,0.40}
\definecolor{CadetBlue}{cmyk}{0.62,0.57,0.23,0}
\usepackage{pdflscape}
\usepackage{relsize}
\usepackage{verbatim}
\usepackage{tabto}
\usepackage{upgreek}
\usepackage{enumitem}

%=================== Settings
\renewcommand{\baselinestretch}{1.2}
\definecolor{gray}{rgb}{0.4 0.4 0.4}
\newcommand{\stimes}{{\times}}

%================== Code syntax highlighting
\lstset{language=C++,frame=ltrb,framesep=2pt,basicstyle=\linespread{0.8} \small,
	keywordstyle=\ttfamily\color{OliveGreen},
	identifierstyle=\ttfamily\color{CadetBlue}\bfseries,
	commentstyle=\color{Brown},
	stringstyle=\ttfamily,
	showstringspaces=true,
	tabsize=2,}

\newcommand{\bOmega}{\mathbf{\Omega}}

\begin{document}

\begin{titlepage}
	\pagestyle{fancy}
	\vspace*{1.0cm}
	\centering
	\vspace{1cm}
	\vspace{.25cm}
	{\Large\bfseries  \DOCSUBJT \par} 
	{\Large\bfseries \DOCTITLE  \par}
	\vspace{1cm}
	{\Large \DOCDATE \par}
	\vspace{1.0cm}
	{\Large Jan Vermaak \par}
	{\Large \DOCREV \par}
	\begin{comment}
		\begin{minipage}[c]{0.45\textwidth}
			\begin{figure}[H]
				
				\includegraphics[width=3in]{Logo2_Medium.png}
			\end{figure}
		\end{minipage}
	\end{comment}

\end{titlepage}	


\pagestyle{fancy}
\rfoot{Page \thepage \ of \pageref{LastPage}}
\cfoot{}
\lfoot{\truncate{14cm}{\DOCTITLE}}
\rhead{}
\chead{\currentname}
\lhead{}
\renewcommand{\footrulewidth}{0.4pt}

\begin{comment}
\tableofcontents
\addtocontents{toc}{~\hfill\textbf{Page}\par}

\listoffigures
\listoftables

\end{comment}
\chead{Weak form}	

%#########################################################################
\newpage
\section{The Weak form of the general Diffusion Equation}
The diffusion solver handles the solution of the diffusion equation of the form

\begin{equation} \label{eq:diffeq}
-\nabla (D\nabla\phi) + \sigma_t \phi = q
\end{equation}
\newline
\noindent
over the domain $\mathbf{\Omega}$ and with any set of boundary conditions on $\partial \mathbf{\Omega}$. This form is suitable for physical processes like heat transfer when $\sigma_t = 0$ and neutron diffusion when $\sigma_t \ne 0$.
\newline \newline
We now proceed with multiplying with $\varphi_i$, a function mapping eq. \ref{eq:diffeq} to a trial space $\mathbf{\Omega}_i$ for which we require that

\begin{equation} \label{eq:trial}
\int_{\bOmega_i} \biggr( 
-\varphi_i\nabla (D\nabla\phi) 
\biggr).dV 
+ \int_{\bOmega_i} ( 
\varphi_i\sigma_t  \phi 
  ).dV
= \int_{\bOmega_i} (\varphi_i q).dV.
\end{equation}
\newline
In this equation we note that, from the product rule we have

\begin{align*}
\frac{d(fg)}{dx} &= \frac{df}{dx}\cdot g + f\cdot \frac{dg}{dx} \\
\therefore 
\int \frac{d(fg)}{dx} .dx&= \int \frac{df}{dx}\cdot g.dx + 
\int f\cdot \frac{dg}{dx}.dx
\end{align*}
\newline
Applying this as an analogy with $f=\varphi$ and $g=D\nabla \phi$ we get

\begin{equation}\label{eq:IntegrateByParts}
\begin{aligned}
\int_{\bOmega_i} \nabla( \varphi_i D\nabla \phi) .dV&=
\int_{\bOmega_i} \nabla \varphi_i\cdot D\nabla \phi.dV + 
\int_{\bOmega_i} \varphi_i\cdot \nabla(D\nabla \phi).dV \\
\therefore
-\int_{\bOmega_i} \biggr(
\varphi_i\cdot \nabla(D\nabla \phi).dV
\biggr) &=
\int_{\bOmega_i} \nabla \varphi_i\cdot D\nabla \phi.dV
-\int_{\bOmega_i} \nabla( \varphi_i D\nabla \phi) .dV
\end{aligned}
\end{equation}
\newline
Now applying Gauss's Divergence theorem on the last term we have
\begin{align}\label{eq:GaussDiv}
\int_{\bOmega_i} \nabla( \varphi_i D\nabla \phi) .dV &=
\int_{\partial \bOmega} \mathbf{\hat{n}}\cdot \varphi_i D\nabla \phi . dA
\end{align}
\newline
which we can place in equation \ref{eq:IntegrateByParts} and then subsequently into equation \ref{eq:trial} which leads to the weak form

\begin{align}\label{eq:weakform}
\int_{\bOmega_i} \biggr(
\nabla \varphi_i\cdot D\nabla \phi
+
\varphi_i\sigma_t  \phi 
\biggr).dV
= \int_{\bOmega_i} (\varphi_i q).dV
+\int_{\partial \bOmega}\biggr( 
\mathbf{\hat{n}}\cdot \varphi_i D\nabla \phi 
\biggr). dA.
\end{align}

\newpage
\chead{Basis functions}
\section{Application of basis functions}
Consider $\phi$ approximated by the contributions of basis functions, $b_j$, and associated coefficients $\phi_j$, i.e.

\begin{align}
\phi \approx \phi_h = \sum_{j=0}^N \phi_j b_j.
\end{align}
\newline
Equation \ref{eq:weakform} now becomes 

\begin{align*}
\int_{\bOmega_i} \biggr(
\nabla \varphi_i\cdot D\nabla (\sum_{j=0}^N \phi_j b_j)
+ 
\varphi_i\sigma_t  (\sum_{j=0}^N \phi_j b_j)
\biggr).dV
&= \int_{\bOmega_i} (\varphi_i q).dV
+\int_{\partial \bOmega}\biggr( 
\mathbf{\hat{n}}\cdot \varphi_i D\nabla (\sum_{j=0}^N \phi_j b_j) 
\biggr). dA 
\end{align*}
\noindent after which we can move the $\nabla$ operator such that
\begin{align*}
\int_{\bOmega_i} \biggr(
\nabla \varphi_i\cdot D (\sum_{j=0}^N \phi_j \nabla b_j)
+ 
\varphi_i\sigma_t  (\sum_{j=0}^N \phi_j b_j)
\biggr).dV
&= \int_{\bOmega_i} (\varphi_i q).dV
+\int_{\partial \bOmega}\biggr( 
\mathbf{\hat{n}}\cdot \varphi_i D (\sum_{j=0}^N \phi_j \nabla b_j) 
\biggr). dA 
\end{align*}
\newline
We now take into account that each integral over a trial space $\bOmega_i$ is a summation over all the elements $\mathcal{K}$ that fall within this space. I.e.
\newline
\newline
\textbf{Trial space i}

\begin{equation*}
\sum^K \biggr[
\int_{\bOmega_k} \biggr(
\nabla \varphi_i\cdot D (\sum_{j=0}^N \phi_j \nabla b_j)
+
\varphi_i\sigma_t  (\sum_{j=0}^N \phi_j b_j)
\biggr).dV
\biggr]
=\sum^K \biggr[
 \int_{\bOmega_i} (\varphi_i q).dV
+\int_{\partial \bOmega}\biggr( 
\mathbf{\hat{n}}\cdot \varphi_i D (\sum_{j=0}^N \phi_j \nabla b_j) 
\biggr). dA \biggr]
\end{equation*}

\newpage
\chead{References}
\begin{thebibliography}{1}
    
    \bibitem{blender} {\em Blender - a 3D modelling and rendering package}, Blender Online Community, Blender Foundation, Blender Institute, Amsterdam, 2018
    
    \bibitem{delaunay} Cheng et al, {\em Delaunay Mesh Generation}, Chapman \& Hall/CRC Computer \& Information Science Series, 2013
    
    
\end{thebibliography}





\end{document}